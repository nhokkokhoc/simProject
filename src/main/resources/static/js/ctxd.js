$(document).ready(function () {

    var homeUrl = "";

    getToken();

    function setToken(token, userName, urlRedirect, menuType) {
        sessionStorage.setItem("token", token);
        sessionStorage.setItem("userName", userName);
        sessionStorage.setItem("urlRedirect", urlRedirect);
        sessionStorage.setItem("menuType", menuType);

    }

    function getToken() {
        var token = sessionStorage.getItem("token");
        if(token == null && (window.location.pathname == homeUrl + '/login'
            || window.location.pathname == homeUrl+ '/register'
            || window.location.pathname == homeUrl+ '/forgot-password'
            || window.location.pathname == homeUrl+ '/reset-password'
            || window.location.pathname == homeUrl+ '/')) {
            return;
        }
        if (token == null && window.location.pathname != homeUrl + '/login') {
            window.location.href = homeUrl + "/login";
        }
        if (token != null && window.location.pathname == homeUrl + '/login') {
            var urlRedirect = sessionStorage.getItem("urlRedirect");
            window.location.href = homeUrl + urlRedirect;
        }
        // set header title
        setHeaderTitle();
    }

    function setHeaderTitle() {
        $.ajax({
            url: homeUrl + "/api/user/profile",
            type: "GET",
            dataType: 'json', headers: {
                'Authorization': sessionStorage.getItem("token"),
            },
            contentType: "application/json",
            success: function (response) {
                //$(".header-name").html("Xin chào: " + response.data.username + " - Hiện còn: " + response.data.point + " Xu | " + response.data.pointLock + " Xu Khóa");
                $(".header-name").html("| Xin chào: " + response.data.username);
                // $("#turnRound").html( response.data.turnRound);
            }
        });
    }

    $("#register").click(function () {
        registerAPI();
    });

    // login function !!!
    function registerAPI() {
        var userName = $("#username").val();
        var password = $("#password").val();
        var email = $("#email").val();
        if(userName == null || password == null || email == null) {
            alert("Các thông tin bắt buộc !!!");
            return;
        }
        if(userName.trim().length < 5) {
            alert("Tài khoản > 6 kí tự !!!");
            return;
        }

        if(password.trim().length < 5) {
            alert("Mật khẩu  > 6 kí tự !!!");
            return;
        }
        var dataReg = {username : userName, password : password, email : email};

        $.ajax({
            url: homeUrl + "/api/user/register",
            type: "POST",
            dataType: 'json',
            contentType: "application/json",
            data: JSON.stringify(dataReg) ,
            success: function (response) {
                alert(response.msg);
                // setToken(response.data.token, response.data.userName);
                window.location.href = homeUrl + "/login";
            },
            error: function(jqXHR) {
                alert("Error: "+ jqXHR.responseJSON.msg);
            }
        });
    }

    $("#login").click(function () {
        loginAPI();
    });

    // login function !!!
    function loginAPI() {
        var userName = $("#username").val();
        var password = $("#password").val();
        if(userName == null || password == null) {
            alert("Tài khoản hoặc mật khẩu không được để trống !!!");
            return;
        }
        if(userName.trim().length < 5) {
            alert("Tài khoản > 6 kí tự !!!");
            return;
        }

        if(password.trim().length < 5) {
            alert("Mật khẩu  > 6 kí tự !!!");
            return;
        }
        var dataLogin = {username : userName, password : password};

        $.ajax({
            url: homeUrl + "/api/login",
            type: "POST",
            dataType: 'json',
            contentType: "application/json",
            data: JSON.stringify(dataLogin) ,
            success: function (response) {
                alert(response.msg);
                setToken(response.data.token, response.data.userName, response.data.urlRedirect, response.data.menuType);
                window.location.href = homeUrl + response.data.urlRedirect;
            },
            error: function(jqXHR) {
                alert("Error: "+ jqXHR.responseJSON.msg);
            }
        });
    }

    $("#forgot-password").click(function (){
        $("#forgot-password").attr("disabled", true);
        forgotPassword();
        $("#forgot-password").attr("disabled", false);
    });
    function forgotPassword() {
        var email = $("#email").val();
        if(email == null || email.trim().length === 0) {
            alert("Please input your email !!!");
            return;
        }
        var dataReset = {email : email};
        $.ajax({
            url: homeUrl + "/api/user/reset-password",
            type: "POST",
            dataType: 'json',
            contentType: "application/json",
            data: JSON.stringify(dataReset) ,
            success: function (response) {
                alert(response.data);
                window.location.href = homeUrl + "/login";
            },
            error: function(jqXHR) {
                alert("Error: "+ jqXHR.responseJSON.msg);
            }
        });

    }

    $("#change-password").click(function (){
        changePassword();
    });
    function changePassword() {
        var code = location.search.split('code=')[1];
        var password = $("#password").val();
        if(password == null || password.trim().length === 0) {
            alert("Please input your password !!!");
            return;
        }

        var repassword = $("#re-password").val();
        if(repassword == null || repassword.trim().length === 0) {
            alert("Please input your re-password !!!");
            return;
        }

        if(password !=repassword) {
            alert("Password and re-password not same !!!");
            return;
        }

        var dataChange = {code : code,password : password};
        $.ajax({
            url: homeUrl + "/api/user/change-password",
            type: "POST",
            dataType: 'json',
            contentType: "application/json",
            data: JSON.stringify(dataChange) ,
            success: function (response) {
                alert(response.msg);
                window.location.href = homeUrl + "/login";
            },
            error: function(jqXHR) {
                alert("Error: "+ jqXHR.responseJSON.msg);
            }
        });

    }

    $("#checkAccount").click(function () {
        checkAccount();
    });

    function checkAccount() {
        var idInGame      = $("#idInGame").val();
        var idServer        = $("#idServer").val();
        var packageCharge   = $("#packageCharge").val();
        if(idInGame == null || idInGame.trim() == "") {
            alert("Vui lòng nhập id ingame!");
            return;
        }
        if(idServer == 0) {
            alert("Chọn server trước!");
            return;
        }
        var dataCheck = {idInGame : idInGame, idServer : idServer};
        $.ajax({
            url: homeUrl + "/api/user/check-account",
            type: "POST",
            dataType: 'json', headers: {
                'Authorization': sessionStorage.getItem("token"),
            },
            contentType: "application/json",
            data: JSON.stringify(dataCheck) ,
            success: function (response) {
                alert(response.msg);
            },
            error: function(jqXHR) {
                alert("Error: "+ jqXHR.responseJSON.msg);
            }
        });
    }
});
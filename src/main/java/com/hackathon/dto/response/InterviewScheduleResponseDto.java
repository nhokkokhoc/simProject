package com.hackathon.dto.response;

import com.hackathon.model.Applicant;
import com.hackathon.model.BaseModel;
import com.hackathon.model.InterviewSchedule;
import com.hackathon.model.ScheduleQuestion;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class InterviewScheduleResponseDto implements Serializable {

    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
    private Integer id;
    private ApplicantResponseDto applicant;
    private String startAt;
    private String roomName;
    private String assessmentOverview;

    public InterviewScheduleResponseDto(InterviewSchedule interviewSchedule) {
        this.id = interviewSchedule.getId();
        this.applicant = new ApplicantResponseDto(interviewSchedule.getApplicant());
        this.startAt = simpleDateFormat.format(interviewSchedule.getStartAt());
        this.roomName = interviewSchedule.getRoomName();
        this.assessmentOverview = interviewSchedule.getAssessmentOverview();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ApplicantResponseDto getApplicant() {
        return applicant;
    }

    public void setApplicant(ApplicantResponseDto applicant) {
        this.applicant = applicant;
    }

    public String getStartAt() {
        return startAt;
    }

    public void setStartAt(String startAt) {
        this.startAt = startAt;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    public String getAssessmentOverview() {
        return assessmentOverview;
    }

    public void setAssessmentOverview(String assessmentOverview) {
        this.assessmentOverview = assessmentOverview;
    }
}

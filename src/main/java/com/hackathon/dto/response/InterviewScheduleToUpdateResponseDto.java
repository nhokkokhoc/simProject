package com.hackathon.dto.response;

import java.io.Serializable;
import java.util.Set;

public class InterviewScheduleToUpdateResponseDto implements Serializable {

    private int aptId;
    private Set<String> interviewIds;
    private String overviewContent;
    private boolean isOnline;
    private String startAt;
    private String endAt;
    private String roomInfo;
    private boolean editAble;

    public int getAptId() {
        return aptId;
    }

    public void setAptId(int aptId) {
        this.aptId = aptId;
    }

    public Set<String> getInterviewIds() {
        return interviewIds;
    }

    public void setInterviewIds(Set<String> interviewIds) {
        this.interviewIds = interviewIds;
    }

    public String getOverviewContent() {
        return overviewContent;
    }

    public void setOverviewContent(String overviewContent) {
        this.overviewContent = overviewContent;
    }

    public boolean isOnline() {
        return isOnline;
    }

    public void setOnline(boolean online) {
        isOnline = online;
    }

    public String getStartAt() {
        return startAt;
    }

    public void setStartAt(String startAt) {
        this.startAt = startAt;
    }

    public String getEndAt() {
        return endAt;
    }

    public void setEndAt(String endAt) {
        this.endAt = endAt;
    }

    public String getRoomInfo() {
        return roomInfo;
    }

    public void setRoomInfo(String roomInfo) {
        this.roomInfo = roomInfo;
    }

    public boolean isEditAble() {
        return editAble;
    }

    public void setEditAble(boolean editAble) {
        this.editAble = editAble;
    }
}

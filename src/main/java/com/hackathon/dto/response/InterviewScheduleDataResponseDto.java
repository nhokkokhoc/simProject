package com.hackathon.dto.response;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;

public class InterviewScheduleDataResponseDto implements Serializable {
    	
    private Integer id;
    private String applicantName;
    private String interviewerNames;
    @JsonFormat(shape = Shape.STRING, pattern = "yyyy-MM-dd HH:mm", timezone = "Asia/Ho_Chi_Minh")
    private Date startAt;
    @JsonFormat(shape = Shape.STRING, pattern = "yyyy-MM-dd HH:mm", timezone = "Asia/Ho_Chi_Minh")
    private Date endAt;
    private String location;
    private Boolean isEdit;

    public InterviewScheduleDataResponseDto(Integer id, String aptName, Date startAt, Date endAt, String location, long count ) {
        this.id = id;
        this.applicantName = aptName;        
        this.startAt = startAt;
        this.endAt = endAt;
        this.location = location;
        this.isEdit = count == 0;
        
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getStartAt() {
        return startAt;
    }

    public void setStartAt(Date startAt) {
        this.startAt = startAt;
    }

	public String getApplicantName() {
		return applicantName;
	}

	public void setApplicantName(String applicantName) {
		this.applicantName = applicantName;
	}

	public String getInterviewerNames() {
		return interviewerNames;
	}

	public void setInterviewerNames(String interviewerNames) {
		this.interviewerNames = interviewerNames;
	}

	public Date getEndAt() {
		return endAt;
	}

	public void setEndAt(Date endAt) {
		this.endAt = endAt;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public Boolean getIsEdit() {
		return isEdit;
	}

	public void setIsEdit(Boolean isEdit) {
		this.isEdit = isEdit;
	}

}

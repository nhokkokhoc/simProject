package com.hackathon.dto.response;

import java.io.Serializable;
import java.util.List;

public class InteviewQuestionResponseDto
        implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer id;
    private List<QuestionData> listQuestion;
    private Boolean result;

    public InteviewQuestionResponseDto (){
    	
    }
    
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

	public List<QuestionData> getListQuestion() {
		return listQuestion;
	}

	public void setListQuestion(List<QuestionData> listQuestion) {
		this.listQuestion = listQuestion;
	}

	public Boolean getResult() {
		return result;
	}

	public void setResult(Boolean result) {
		this.result = result;
	}

}

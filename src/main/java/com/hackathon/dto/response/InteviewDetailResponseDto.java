package com.hackathon.dto.response;

import java.io.Serializable;
import java.util.List;

public class InteviewDetailResponseDto
        implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer id;
    private List<QuestionData> listQuestion;
    private List<AssessmentInfo> assessmentList;
    private Boolean result;

    public InteviewDetailResponseDto (){
    	
    }
    
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

	public List<QuestionData> getListQuestion() {
		return listQuestion;
	}

	public void setListQuestion(List<QuestionData> listQuestion) {
		this.listQuestion = listQuestion;
	}

	public Boolean getResult() {
		return result;
	}

	public void setResult(Boolean result) {
		this.result = result;
	}
	
	public List<AssessmentInfo> getAssessmentList() {
		return assessmentList;
	}

	public void setAssessmentList(List<AssessmentInfo> assessmentList) {
		this.assessmentList = assessmentList;
	}

	public static class AssessmentInfo implements Serializable{

		private static final long serialVersionUID = 1L;
		
		private String interviewerName;
		private String assessment;
		private String result;
		
		public String getInterviewerName() {
			return interviewerName;
		}
		public void setInterviewerName(String interviewerName) {
			this.interviewerName = interviewerName;
		}
		public String getAssessment() {
			return assessment;
		}
		public void setAssessment(String assessment) {
			this.assessment = assessment;
		}
		public String getResult() {
			return result;
		}
		public void setResult(String result) {
			this.result = result;
		}
		
	}

}

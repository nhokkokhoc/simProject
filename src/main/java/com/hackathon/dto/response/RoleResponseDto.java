package com.hackathon.dto.response;

import com.hackathon.model.Role;

import java.io.Serializable;

public class RoleResponseDto
        implements Serializable {
    private static final long serialVersionUID = 3756909113144287755L;
    private Integer id;
    private String name;

    public RoleResponseDto(Role role) {
        this.id = role.getId();
        this.name = role.getName();
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

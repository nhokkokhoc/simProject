package com.hackathon.dto.response;


import com.hackathon.model.BaseModel;
import com.hackathon.model.Category;
import com.hackathon.model.Question;

import javax.persistence.*;
import java.io.Serializable;

public class QuestionResponseDto implements Serializable {

    private Integer id;
    private String questionContent;
    private String answer;
    private Integer level;

    public QuestionResponseDto(Question question) {
        this.id = question.getId();
        this.questionContent = question.getQuestionContent();
        this.answer = question.getAnswer();
        this.level = question.getLevel();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getQuestionContent() {
        return questionContent;
    }

    public void setQuestionContent(String questionContent) {
        this.questionContent = questionContent;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }
}

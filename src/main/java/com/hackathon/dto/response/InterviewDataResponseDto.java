package com.hackathon.dto.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class InterviewDataResponseDto
        implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    @Id
    private Integer id;
    private Integer aptId;
    private String fullName;
    private String interviewTime;
    private String location;
    private String overview;
    private String other;
    private String cvLink;

    public InterviewDataResponseDto() {

    }

    public InterviewDataResponseDto(Integer id, Integer aptId, String fullName,
                                    String interviewTime, String location, String overview, String other) {
        this.id = id;
        this.aptId = aptId;
        this.fullName = fullName;
        this.interviewTime = interviewTime;
        this.location = location;
        this.overview = overview;
        this.other = other;
    }

    public Integer getAptId() {
        return aptId;
    }

    public void setAptId(Integer aptId) {
        this.aptId = aptId;
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getInterviewTime() {
        return interviewTime;
    }

    public void setInterviewTime(String interviewTime) {
        this.interviewTime = interviewTime;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public String getOther() {
        return other;
    }

    public void setOther(String other) {
        this.other = other;
    }

    public String getCvLink() {
        return cvLink;
    }

    public void setCvLink(String cvLink) {
        this.cvLink = cvLink;
    }

}

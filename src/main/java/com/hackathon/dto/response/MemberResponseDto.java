package com.hackathon.dto.response;

import com.hackathon.model.RoleEnum;
import com.hackathon.model.User;

import java.io.Serializable;

public class MemberResponseDto implements Serializable {
    private int id;
    private String userName;
    private String email;
    private String phone;
    private String userType;
    private String fullName;

    public MemberResponseDto(User user) {
        this.id = user.getId();
        this.userName = user.getUsername();
        this.email = user.getEmail();
        this.phone = user.getMobile();
        this.userType = RoleEnum.getRoleByValue(user.getRole().getId()).getName();
        this.fullName = user.getFullName();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }
}

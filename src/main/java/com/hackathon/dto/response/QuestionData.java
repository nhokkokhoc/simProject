package com.hackathon.dto.response;

import java.io.Serializable;

public class QuestionData  implements Serializable  {

	private Integer id;
    private String question;
    private String answer;
    private String aptAnswer;
    private String rate;
    private Boolean passFail;
    private String interviewerName;
    
    public QuestionData(String question, String answer, String aptAnswer, Integer rate) {
    	this.question = question;
    	this.answer = answer;
    	this.aptAnswer = aptAnswer;
    	this.rate = rate.toString();
    }
    
    public QuestionData(String question, String answer, String aptAnswer, Integer rate, String interviewerName ) {
    	this.question = question;
    	this.answer = answer;
    	this.aptAnswer = aptAnswer;
    	this.rate = rate.toString();
    	this.interviewerName = interviewerName;
    }
    
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getQuestion() {
		return question;
	}
	public void setQuestion(String question) {
		this.question = question;
	}
	public String getAnswer() {
		return answer;
	}
	public void setAnswer(String answer) {
		this.answer = answer;
	}
	public String getAptAnswer() {
		return aptAnswer;
	}
	public void setAptAnswer(String aptAnswer) {
		this.aptAnswer = aptAnswer;
	}
	public String getRate() {
		return rate;
	}
	public void setRate(String rate) {
		this.rate = rate;
	}
	public Boolean getPassFail() {
		return passFail;
	}
	public void setPassFail(Boolean passFail) {
		this.passFail = passFail;
	}

	public String getInterviewerName() {
		return interviewerName;
	}

	public void setInterviewerName(String interviewerName) {
		this.interviewerName = interviewerName;
	}
}
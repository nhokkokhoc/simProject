package com.hackathon.dto.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

@Entity
public class InteviewerResponseDto
        implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    @Id
    private Integer id;
    private String name;
    private String location;
    private String status;
    @JsonFormat(shape = Shape.STRING, pattern = "yyyy-MM-dd HH:mm", timezone = "Asia/Ho_Chi_Minh")
    private Date schedule;

    public InteviewerResponseDto(Integer id, String name, String location, Date schedule, Integer result) {
        this.id = id;
        this.name = name;
        this.location = location;
        this.schedule = schedule;
        Date date = new Date();
        this.status = result == 0 && date.after(schedule) ? ResultEnum.MISS.getName() : ResultEnum.getNameFromValue(result);
    }

    public InteviewerResponseDto() {

    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return this.location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getSchedule() {
        return this.schedule;
    }

    public void setSchedule(Date schedule) {
        this.schedule = schedule;
    }


}

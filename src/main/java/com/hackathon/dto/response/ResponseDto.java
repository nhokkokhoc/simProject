package com.hackathon.dto.response;

import java.io.Serializable;

public class ResponseDto<T>
        implements Serializable {
    private int status;
    private String msg;
    private T data;

    public ResponseDto() {
    }

    public ResponseDto(int status, String msg) {
        this.status = status;
        this.msg = msg;
    }

    public ResponseDto(int status, String msg, T data) {
        this.status = status;
        this.msg = msg;
        this.data = data;
    }

    public int getStatus() {
        return this.status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMsg() {
        return this.msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return (T) this.data;
    }

    public void setData(T data) {
        this.data = data;
    }
}

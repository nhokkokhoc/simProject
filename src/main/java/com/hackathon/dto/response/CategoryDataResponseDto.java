package com.hackathon.dto.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.hackathon.model.Category;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import com.hackathon.model.*;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class CategoryDataResponseDto
        implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	private Integer id;
    private String name;
    private String group;
    private Long totalQuestion;
    @JsonFormat(shape = Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/Ho_Chi_Minh")
    private Date createAt;

	public CategoryDataResponseDto (Category category) {
		this.id = category.getId();
		this.name = category.getName();
		this.group = category.getGroup();
	}


    public CategoryDataResponseDto (Integer id, String name, String group, Date createAt, Long total) {
    	this.id = id;
    	this.name = name;
    	this.group = group;
    	for(GroupEnum en: GroupEnum.values()) {
    		if(en.toString().equals(group)) {
    			this.group = en.getLabel();
    			break;
    		}
    	}
    	this.createAt = createAt; 
    	this.totalQuestion = total;
    }

	public CategoryDataResponseDto() {

	}

	public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}

	public Long getTotalQuestion() {
		return totalQuestion;
	}

	public void setTotalQuestion(Long totalQuestion) {
		this.totalQuestion = totalQuestion;
	}
	
}

package com.hackathon.dto.response;

import com.hackathon.model.Applicant;
import com.hackathon.model.BaseModel;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

public class ApplicantResponseDto implements Serializable {

  private Integer id;
  private String fullName;
  private String cvPath;

  public ApplicantResponseDto(Applicant applicant) {
    this.id = applicant.getId();
    this.fullName = applicant.getFullName();
    this.cvPath = applicant.getCvPath();
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getFullName() {
    return fullName;
  }

  public void setFullName(String fullName) {
    this.fullName = fullName;
  }

  public String getCvPath() {
    return cvPath;
  }

  public void setCvPath(String cvPath) {
    this.cvPath = cvPath;
  }
}

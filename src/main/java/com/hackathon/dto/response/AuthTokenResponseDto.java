package com.hackathon.dto.response;

import java.io.Serializable;

public class AuthTokenResponseDto
        implements Serializable {
    private static final long serialVersionUID = 1L;
    private String token;
    private String userName;
    private String urlRedirect;
    private int menuType;

    public AuthTokenResponseDto() {
    }

    public AuthTokenResponseDto(String token, String userName, String urlRedirect, int menuType) {
        this.token = ("Bearer " + token);
        this.userName = userName;
        this.urlRedirect = urlRedirect;
        this.menuType = menuType;
    }

    public String getToken() {
        return this.token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUserName() {
        return this.userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUrlRedirect() {
        return urlRedirect;
    }

    public void setUrlRedirect(String urlRedirect) {
        this.urlRedirect = urlRedirect;
    }

    public int getMenuType() {
        return menuType;
    }

    public void setMenuType(int menuType) {
        this.menuType = menuType;
    }
}

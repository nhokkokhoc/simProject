package com.hackathon.dto.response;

import com.hackathon.model.User;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;

@Entity
public class InteviewerDataResponseDto
        implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	private Integer id;
    private String name;
    private Long totalSchedule;

    public InteviewerDataResponseDto (Integer id, String name, Long total) {
    	this.id = id;
    	this.name = name;
    	this.setTotalSchedule(total);
    }

    public InteviewerDataResponseDto(User user) {
        this.id = user.getId();
        this.name = user.getFullName();
    }

    public InteviewerDataResponseDto() {

    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

	public Long getTotalSchedule() {
		return totalSchedule;
	}

	public void setTotalSchedule(Long totalSchedule) {
		this.totalSchedule = totalSchedule;
	}


}

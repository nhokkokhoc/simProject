package com.hackathon.dto.response;

import java.util.HashMap;
import java.util.Map;

public enum ResultEnum {
    PENDING(0, "PENDING"), PASS(1, "PASS"), FAIL(2, "FAIL"), MISS(3, "MISS");

    private int value;

    private String name;

    ResultEnum(int value, String name) {
        this.value = value;
        this.name = name;
    }

    private static Map<Integer, String> mMap;

    public int getValue() {
        return value;
    }

    public String getName() {
        return name;
    }
    public static String getNameFromValue(Integer value) {
        if (mMap == null) {
            initializeMapping();
        }
        if (mMap.containsKey(value)) {
            return mMap.get(value);
        }
        return null;
    }

    private static void initializeMapping() {
        mMap = new HashMap<>();
        for (ResultEnum s : ResultEnum.values()) {
            mMap.put(s.getValue(), s.getName());
        }
    }

}
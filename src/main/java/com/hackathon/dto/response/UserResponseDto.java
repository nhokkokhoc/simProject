package com.hackathon.dto.response;

import com.hackathon.model.User;

import java.io.Serializable;

public class UserResponseDto implements Serializable {
    private static final long serialVersionUID = 7156526077883281623L;
    private Integer id;
    private String username;
    private String email;
    private String roleName;
    private int point;
    private int pointLock;
    private int turnRound;

    public UserResponseDto(final User user) {
        this.id = user.getId();
        this.username = user.getUsername();
        this.email = user.getEmail();
        this.roleName = "USER";
        this.point = user.getPoint();
        this.pointLock = user.getPointLock();
        this.turnRound = user.getTurnRound();
    }

    public static long getSerialVersionUID() {
        return 7156526077883281623L;
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(final Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(final String username) {
        this.username = username;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(final String email) {
        this.email = email;
    }

    public String getRoleName() {
        return this.roleName;
    }

    public void setRoleName(final String roleName) {
        this.roleName = roleName;
    }

    public int getPoint() {
        return this.point;
    }

    public void setPoint(final int point) {
        this.point = point;
    }

    public int getPointLock() {
        return pointLock;
    }

    public void setPointLock(int pointLock) {
        this.pointLock = pointLock;
    }

    public int getTurnRound() {
        return turnRound;
    }

    public void setTurnRound(int turnRound) {
        this.turnRound = turnRound;
    }
}
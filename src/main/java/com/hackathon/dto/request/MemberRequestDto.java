package com.hackathon.dto.request;

import com.hackathon.model.User;

import java.io.Serializable;

public class MemberRequestDto implements Serializable {
    private String userName;
    private String email;
    private String phone;
    private Integer permission;
    private String fullName;

    public MemberRequestDto() {
    }

    public MemberRequestDto(User user) {
        this.userName = user.getUsername();
        this.email = user.getEmail();
        this.phone = user.getMobile();
        this.permission = user.getRole().getId();
        this.fullName = user.getFullName();
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Integer getPermission() {
        return permission;
    }

    public void setPermission(Integer permission) {
        this.permission = permission;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }
}

package com.hackathon.dto.request;

import java.io.Serializable;

public class CategoryRequestDto
        implements Serializable {
    private static final long serialVersionUID = 7156526077883281623L;
    private String id;
    private String group;
    private String name;
    
    public static long getSerialVersionUID() {
        return 7156526077883281623L;
    }

	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

}

package com.hackathon.dto.request;

import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

public class AuthenticationRequestDto
        implements Serializable
{
    @ApiModelProperty(example="username")
    private String username;
    @ApiModelProperty(example="password")
    private String password;

    public String getUsername()
    {
        return this.username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getPassword()
    {
        return this.password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public String toString()
    {
        return "AuthenticationRequestDto{username='" + this.username + '\'' + ", password='" + this.password + '\'' + '}';
    }
}

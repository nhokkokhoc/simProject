package com.hackathon.dto.request;

import java.io.Serializable;

public class RoleCreateRequestDto
        implements Serializable {
    private static final long serialVersionUID = 3756909113144287755L;
    private String name;

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String toString() {
        return "RoleCreateRequestDto{name='" + this.name + '\'' + '}';
    }
}

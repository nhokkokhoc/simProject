package com.hackathon.dto.request;

import java.io.Serializable;

public class RateUpdateRequestDto
        implements Serializable {
    private String valueApp;
    private String description;

    public String getValueApp() {
        return this.valueApp;
    }

    public void setValueApp(String valueApp) {
        this.valueApp = valueApp;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}

package com.hackathon.dto.request;

import com.hackathon.anotation.FieldRequired;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.Date;

import org.springframework.web.multipart.MultipartFile;

public class uploadCvRequestDto
        implements Serializable {
    private static final long serialVersionUID = 7156526077883281623L;
    
    private MultipartFile cvFile;


    public static long getSerialVersionUID() {
        return 7156526077883281623L;
    }

	public MultipartFile getMultipartFile() {
		return cvFile;
	}

	public void setMultipartFile(MultipartFile cvFile) {
		this.cvFile = cvFile;
	}
}

package com.hackathon.dto.request;

import com.hackathon.anotation.FieldRequired;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

public class ChangePasswordRequestDto
        implements Serializable
{
    private static final long serialVersionUID = 7156526077883281623L;
    @FieldRequired
    @ApiModelProperty(example="admin", position=2)
    private String newPassword;
    @FieldRequired
    @ApiModelProperty(example="admin", position=3)
    private String reNewPassword;

    public String getNewPassword()
    {
        return this.newPassword;
    }

    public void setNewPassword(String newPassword)
    {
        this.newPassword = newPassword;
    }

    public String getReNewPassword()
    {
        return this.reNewPassword;
    }

    public void setReNewPassword(String reNewPassword)
    {
        this.reNewPassword = reNewPassword;
    }

    public String toString()
    {
        return "ChangePasswordRequestDto{newPassword='" + this.newPassword + '\'' + ", reNewPassword='" + this.reNewPassword + '\'' + '}';
    }
}

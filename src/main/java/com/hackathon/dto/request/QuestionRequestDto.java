package com.hackathon.dto.request;

import java.io.Serializable;

public class QuestionRequestDto
        implements Serializable {
    private static final long serialVersionUID = 7156526077883281623L;
    private String id;
    private String question;
    private String answer;
    private String level;
    private String categoryId;
    
    public static long getSerialVersionUID() {
        return 7156526077883281623L;
    }

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public String getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}

}

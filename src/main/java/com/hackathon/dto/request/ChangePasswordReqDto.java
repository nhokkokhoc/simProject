package com.hackathon.dto.request;

import java.io.Serializable;

public class ChangePasswordReqDto
        implements Serializable
{
    private static final long serialVersionUID = 7156526077883281623L;
    private String code;

    private String password;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}

package com.hackathon.dto.request;

import java.io.Serializable;
import java.util.List;

public class InterviewScheduleResultRequestDto  implements Serializable {

    private String assessmentOverView;

    private List<ScheduleQuestionResultDto> scheduleQuestion;

    private String other;

    private boolean result;

    public String getAssessmentOverView() {
        return assessmentOverView;
    }

    public void setAssessmentOverView(String assessmentOverView) {
        this.assessmentOverView = assessmentOverView;
    }

    public List<ScheduleQuestionResultDto> getScheduleQuestion() {
        return scheduleQuestion;
    }

    public void setScheduleQuestion(List<ScheduleQuestionResultDto> scheduleQuestion) {
        this.scheduleQuestion = scheduleQuestion;
    }

    public String getOther() {
        return other;
    }

    public void setOther(String other) {
        this.other = other;
    }

    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }
}

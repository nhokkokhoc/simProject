package com.hackathon.dto.request;

import java.io.Serializable;

public class ResetPasswordRequestDto implements Serializable {
    private static final long serialVersionUID = 7156526077883281623L;

    private String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public ResetPasswordRequestDto(String email) {
        this.email = email;
    }

    public ResetPasswordRequestDto() {
    }
}

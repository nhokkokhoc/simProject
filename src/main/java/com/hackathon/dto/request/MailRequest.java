package com.hackathon.dto.request;

import java.io.Serializable;

public class MailRequest implements Serializable {
    private String name;
    private String to;
    private String from;
    private String subject;

    public MailRequest(String to, String subject) {
        this.to = to;
        this.subject = subject;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }
}

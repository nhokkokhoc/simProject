package com.hackathon.dto.request;

import java.io.Serializable;

public class ScheduleQuestionResultDto implements Serializable {
    private int questionId;
    private String aptAnswer;
    private int rate;

    public int getQuestionId() {
        return questionId;
    }

    public void setQuestionId(int questionId) {
        this.questionId = questionId;
    }

    public String getAptAnswer() {
        return aptAnswer;
    }

    public void setAptAnswer(String aptAnswer) {
        this.aptAnswer = aptAnswer;
    }

    public int getRate() {
        return rate;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }
}

package com.hackathon.dto.request;

import com.hackathon.anotation.FieldRequired;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

public class UserRequestDto
        implements Serializable {
    private static final long serialVersionUID = 7156526077883281623L;
    @FieldRequired
    @ApiModelProperty(example = "admin", position = 1)
    private String username;
    @FieldRequired
    @ApiModelProperty(example = "admin", position = 2)
    private String password;
    @FieldRequired
    @ApiModelProperty(example = "dokd@erpvn.biz", position = 5)
    private String email;

    private String ipAddress;


    public static long getSerialVersionUID() {
        return 7156526077883281623L;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }
}

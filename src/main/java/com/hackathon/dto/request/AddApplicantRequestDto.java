package com.hackathon.dto.request;

import com.hackathon.anotation.FieldRequired;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.Date;

import org.springframework.web.multipart.MultipartFile;

public class AddApplicantRequestDto
        implements Serializable {
    private static final long serialVersionUID = 7156526077883281623L;
    private String id;
    private String fullname;
    private String email;
    private Date birthday;
    private String phone;


    public static long getSerialVersionUID() {
        return 7156526077883281623L;
    }

    public String getFullname() {
        return this.fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
}

package com.hackathon.repository;

import com.hackathon.model.Role;
import com.hackathon.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public abstract interface UserRepository
        extends JpaRepository<User, Integer> {
    Optional<UserDetails> findByUsername(String paramString);

    @Query("select u from User u where u.username =:userName")
     User getUserInfo(@Param("userName") String paramString);

    List<User> findByRole(Role paramRole);

    Optional<User> findByEmail(String email);

    Optional<User> findBySecretCode(String secretCode);
}

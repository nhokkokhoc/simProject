package com.hackathon.repository;

import com.hackathon.dto.response.CategoryDataResponseDto;
import com.hackathon.model.Category;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Integer> {
    
    @Query("select new com.hackathon.dto.response.CategoryDataResponseDto ("
    		+ " ca.id, ca.name, ca.group, ca.createdOn, count(qu.id)) "
    		+ " from Question qu right join qu.category ca "
    		+ " group by ca.id, ca.name, ca.group, ca.createdOn ")
    List<CategoryDataResponseDto> findAllCategory();
}

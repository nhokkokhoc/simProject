package com.hackathon.repository;

import com.hackathon.model.AppProperties;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public abstract interface AppPropertiesRepository extends JpaRepository<AppProperties, Integer> {
    public abstract AppProperties findByKeyApp(String paramString);
}

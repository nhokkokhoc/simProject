package com.hackathon.repository;

import com.hackathon.dto.response.InterviewScheduleDataResponseDto;
import com.hackathon.dto.response.InteviewerResponseDto;
import com.hackathon.model.InterviewSchedule;
import com.hackathon.model.User;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface InterviewScheduleRepository extends JpaRepository<InterviewSchedule, Integer> {
	
	@Query("select "
    		+ " new com.hackathon.dto.response.InterviewScheduleDataResponseDto(ins.id , "
    		+ " apt.fullName, ins.startAt, ins.endAt, ins.roomName, count(sq.id)) "
    		+ " from PeopleSchedule pes "
    		+ " right join pes.interviewSchedule ins "
    		+ " left join ins.scheduleQuestions sq"
    		+ " left join ins.applicant apt "
    		+ " group by ins.id , apt.fullName, ins.startAt, ins.endAt, ins.roomName ")
    List<InterviewScheduleDataResponseDto> findAllData();

	@Query("select u "
    		+ " from PeopleSchedule pes "
    		+ " inner join pes.interviewSchedule ins "
    		+ " inner join pes.user u"
    		+ " where ins.id = :id ")
    List<User> findInteviewer(@Param("id") Integer id);
}

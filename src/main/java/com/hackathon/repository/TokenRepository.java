package com.hackathon.repository;

import com.hackathon.model.Token;
import org.springframework.data.repository.CrudRepository;

import java.util.Date;
import java.util.List;

public abstract interface TokenRepository extends CrudRepository<Token, Integer> {
    public abstract Token findByToken(String paramString);

    public abstract List<Token> findByExpDateBeforeAndActiveFalse(Date paramDate);
}

package com.hackathon.repository;

import com.hackathon.dto.response.InteviewerDataResponseDto;
import com.hackathon.dto.response.InteviewerResponseDto;
import java.util.List;

import com.hackathon.model.InterviewSchedule;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface InteviewerRepository extends JpaRepository<InteviewerResponseDto, Integer> {

    @Query("select "
    		+ " new com.hackathon.dto.response.InteviewerResponseDto(ins.id , "
    		+ " apt.fullName, ins.roomName, ins.startAt, pes.result) "
    		+ " from PeopleSchedule pes inner join pes.user u  "
    		+ " inner join pes.interviewSchedule ins "
    		+ " inner join ins.applicant apt"
    		+ " where u.id= :userId ")
    List<InteviewerResponseDto> findByUserId(@Param("userId") Integer userId);
    
    @Query("select "
    		+ " new com.hackathon.dto.response.InteviewerResponseDto(ins.id , "
    		+ " apt.fullName, ins.roomName, ins.startAt, pes.result) "
    		+ " from PeopleSchedule pes inner join pes.user u  "
    		+ " inner join pes.interviewSchedule ins "
    		+ " inner join ins.applicant apt")
    List<InteviewerResponseDto> findByAdmin();

    @Query("select new com.hackathon.dto.response.InteviewerDataResponseDto ("
    		+ " u.id, u.username, count(pes.id)) "
    		+ " from PeopleSchedule pes inner join pes.user u "
    		+ " inner join u.role r"
    		+ " where r.id = 3 "
    		+ " group by u.id, u.username ")
    List<InteviewerDataResponseDto> findAllInterviewer();

}

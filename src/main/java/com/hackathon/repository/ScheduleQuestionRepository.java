package com.hackathon.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.hackathon.dto.response.QuestionData;
import com.hackathon.model.ScheduleQuestion;

@Repository
public interface ScheduleQuestionRepository extends JpaRepository<ScheduleQuestion, Integer> {



    @Query("select new com.hackathon.dto.response.QuestionData("
    		+ " qu.questionContent, qu.answer, sq.aptAnswer, sq.assessment ) "
    		+ " from ScheduleQuestion sq "
    		+ " inner join sq.question qu "
    		+ "	inner join sq.interviewSchedule ins "
    		+ " inner join sq.peopleSchedule pos "
    		+ " inner join pos.user us "
    		+ " where ins.id = :interviewId and us.id = :userId")
    List<QuestionData> findQuestionOfInteviewer(@Param("interviewId") Integer interviewId, @Param("userId") Integer userId);

    @Query("select new com.hackathon.dto.response.QuestionData("
    		+ " qu.questionContent, qu.answer, sq.aptAnswer, sq.assessment, us.fullName) "
    		+ " from ScheduleQuestion sq "
    		+ " inner join sq.question qu "
    		+ "	inner join sq.interviewSchedule ins"
    		+ " inner join sq.peopleSchedule ps "
    		+ " inner join ps.user us "
    		+ " where ins.id = :id ")
    List<QuestionData> findQuestionOfInteview(@Param("id") Integer id);
}

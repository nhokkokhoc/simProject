package com.hackathon.repository;

import com.hackathon.model.InterviewSchedule;
import com.hackathon.model.PeopleSchedule;
import com.hackathon.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PeopleScheduleRepository extends JpaRepository<PeopleSchedule, Integer> {

    Optional<PeopleSchedule> findByUserAndInterviewSchedule(User user, InterviewSchedule interviewSchedule);

    List<PeopleSchedule> findByInterviewSchedule(InterviewSchedule interviewSchedule);

    void deleteByInterviewSchedule(InterviewSchedule interviewSchedule);
}

package com.hackathon.repository;

import com.hackathon.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public abstract interface RoleRepository extends JpaRepository<Role, Integer> {
    public abstract Role findByName(String paramString);
}

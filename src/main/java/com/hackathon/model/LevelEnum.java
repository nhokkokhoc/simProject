package com.hackathon.model;

public enum LevelEnum {
	Beginner(1),
	Junior(2),
	Advance(3);

    public final Integer label;

    private LevelEnum (Integer label) {
        this.label = label;
    }
    
    public Integer getLabel() {
        return label;
    }
}

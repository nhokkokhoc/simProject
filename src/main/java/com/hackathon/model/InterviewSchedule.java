package com.hackathon.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "interview_schedule")
public class InterviewSchedule extends BaseModel implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @OneToOne(cascade = {javax.persistence.CascadeType.MERGE}, fetch = FetchType.EAGER)
    @JoinColumn(name = "applicant_id")
    private Applicant applicant;
    private Date startAt;
    private Date endAt;
    private String roomName;
    private String assessmentOverview;

    private boolean isOnline;

    @OneToMany(mappedBy = "interviewSchedule", cascade = CascadeType.ALL)
    List<ScheduleQuestion> scheduleQuestions;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Applicant getApplicant() {
        return applicant;
    }

    public void setApplicant(Applicant applicant) {
        this.applicant = applicant;
    }

    public Date getStartAt() {
        return startAt;
    }

    public void setStartAt(Date startAt) {
        this.startAt = startAt;
    }

    public Date getEndAt() {
        return endAt;
    }

    public void setEndAt(Date endAt) {
        this.endAt = endAt;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    public String getAssessmentOverview() {
        return assessmentOverview;
    }

    public void setAssessmentOverview(String assessmentOverview) {
        this.assessmentOverview = assessmentOverview;
    }

    public boolean isOnline() {
        return isOnline;
    }

    public void setOnline(boolean online) {
        isOnline = online;
    }

    public List<ScheduleQuestion> getScheduleQuestions() {
        return scheduleQuestions;
    }

    public void setScheduleQuestions(List<ScheduleQuestion> scheduleQuestions) {
        this.scheduleQuestions = scheduleQuestions;
    }
}

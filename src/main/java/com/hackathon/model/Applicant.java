package com.hackathon.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "applicant")
public class Applicant extends BaseModel implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;
  private String fullName;
  @DateTimeFormat(pattern = "yyyy-MM-dd")
  private Date birthday;
  private String phone;
  private String email;
  private String cvPath;
  private boolean hasInterview;
  private String overviewContent;

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getFullName() {
    return fullName;
  }

  public void setFullName(String fullName) {
    this.fullName = fullName;
  }

  public Date getBirthday() {
    return birthday;
  }

  public void setBirthday(Date birthday) {
    this.birthday = birthday;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getCvPath() {
    return cvPath;
  }

  public void setCvPath(String cvPath) {
    this.cvPath = cvPath;
  }

  public boolean getHasInterview() {
    return hasInterview;
  }

  public void setHasInterview(boolean hasInterview) {
    this.hasInterview = hasInterview;
  }

  public String getOverviewContent() {
    return overviewContent;
  }

  public void setOverviewContent(String overviewContent) {
    this.overviewContent = overviewContent;
  }

}

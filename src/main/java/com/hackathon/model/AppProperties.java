package com.hackathon.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "app_properties")
public class AppProperties extends BaseModel implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String keyApp;
    private String valueApp;
    private String description;

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getKeyApp() {
        return this.keyApp;
    }

    public void setKeyApp(String keyApp) {
        this.keyApp = keyApp;
    }

    public String getValueApp() {
        return this.valueApp;
    }

    public void setValueApp(String valueApp) {
        this.valueApp = valueApp;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}

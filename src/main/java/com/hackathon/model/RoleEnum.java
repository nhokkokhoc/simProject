package com.hackathon.model;

public enum RoleEnum {
    ADMIN(1, "ADMIN"), HR(2, "HR"), INTERVIEW(3, "INTERVIEW");

    private int value;

    private String name;

    RoleEnum(int value, String name) {
        this.value = value;
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public String getName() {
        return name;
    }

    public static RoleEnum getRoleByValue(final int value) {

        for (RoleEnum roleENum : RoleEnum.values()) {
            if (value == roleENum.value) {
                return roleENum;
            }
        }
        return null;
    }
}

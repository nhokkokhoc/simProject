package com.hackathon.model;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

@Entity
@Table(name = "users")
public class User implements Serializable, UserDetails {
    private static final long serialVersionUID = 7156526077883281623L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id", nullable = false)
    private Integer id;
    @Column(name = "user_name", nullable = false)
    private String username;
    @Column(name = "password", nullable = false)
    private String password;
    @Column(name = "email", nullable = false)
    private String email;
    @OneToOne(cascade = {javax.persistence.CascadeType.MERGE}, fetch = FetchType.EAGER)
    @JoinColumn(name = "role_id")
    private Role role;
    private int point;
    private Date lastTransfer;

    private String passwordGame;

    @Column(name = "point_lock")
    private int pointLock;

    private String appkey;
    private String token;
    private String qltoken;
    private String imei;
    private String mobile;
    @Column(name = "turn_round")
    private int turnRound;

    @Column(name = "last_draw")
    private Date lastDraw;

    private String secretCode;

    private Date lastGenCode;

    private boolean delFlag;

    private String fullName;


    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Collection<? extends GrantedAuthority> getAuthorities() {
        return new ArrayList();
    }

    public String getPassword() {
        return this.password;
    }

    public String getUsername() {
        return this.username;
    }

    public boolean isAccountNonExpired() {
        return true;
    }

    public boolean isAccountNonLocked() {
        return true;
    }

    public boolean isCredentialsNonExpired() {
        return true;
    }

    public boolean isEnabled() {
        return true;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Role getRole() {
        return this.role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public int getPoint() {
        return this.point;
    }

    public void setPoint(int point) {
        this.point = point;
    }


    public Date getLastTransfer() {
        return this.lastTransfer;
    }

    public void setLastTransfer(Date lastTransfer) {
        this.lastTransfer = lastTransfer;
    }

    public String getPasswordGame() {
        return passwordGame;
    }

    public void setPasswordGame(String passwordGame) {
        this.passwordGame = passwordGame;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getAppkey() {
        return appkey;
    }

    public void setAppkey(String appkey) {
        this.appkey = appkey;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getQltoken() {
        return qltoken;
    }

    public void setQltoken(String qltoken) {
        this.qltoken = qltoken;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public int getPointLock() {
        return pointLock;
    }

    public void setPointLock(int pointLock) {
        this.pointLock = pointLock;
    }

    public int getTurnRound() {
        return turnRound;
    }

    public void setTurnRound(int turnRound) {
        this.turnRound = turnRound;
    }

    public Date getLastDraw() {
        return lastDraw;
    }

    public void setLastDraw(Date lastDraw) {
        this.lastDraw = lastDraw;
    }

    public String getSecretCode() {
        return secretCode;
    }

    public void setSecretCode(String secretCode) {
        this.secretCode = secretCode;
    }

    public Date getLastGenCode() {
        return lastGenCode;
    }

    public void setLastGenCode(Date lastGenCode) {
        this.lastGenCode = lastGenCode;
    }

    public boolean isDelFlag() {
        return delFlag;
    }

    public void setDelFlag(boolean delFlag) {
        this.delFlag = delFlag;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }
}

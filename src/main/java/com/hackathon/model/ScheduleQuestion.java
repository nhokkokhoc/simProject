package com.hackathon.model;


import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "schedule_question")
public class ScheduleQuestion extends BaseModel implements Serializable {

  public ScheduleQuestion(Integer id) {
    this.id = id;
  }

  public ScheduleQuestion() {
  }

  @Id
  @GeneratedValue(strategy=GenerationType.IDENTITY)
  private Integer id;

  @OneToOne(cascade = {javax.persistence.CascadeType.MERGE}, fetch = FetchType.EAGER)
  @JoinColumn(name = "people_schedule_id")
  private PeopleSchedule peopleSchedule;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "question_id")
  private Question question;

  @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
  @JoinColumn(name = "interview_schedule_id")
  private InterviewSchedule interviewSchedule;

  private String aptAnswer;

  private int assessment;

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public InterviewSchedule getInterviewSchedule() {
    return interviewSchedule;
  }

  public void setInterviewSchedule(InterviewSchedule interviewSchedule) {
    this.interviewSchedule = interviewSchedule;
  }

  public String getAptAnswer() {
    return aptAnswer;
  }

  public void setAptAnswer(String aptAnswer) {
    this.aptAnswer = aptAnswer;
  }

  public int getAssessment() {
    return assessment;
  }

  public void setAssessment(int assessment) {
    this.assessment = assessment;
  }

public Question getQuestion() {
	return question;
}

public void setQuestion(Question question) {
	this.question = question;
}

public PeopleSchedule getPeopleSchedule() {
  return peopleSchedule;
}

public void setPeopleSchedule(PeopleSchedule peopleSchedule) {
  this.peopleSchedule = peopleSchedule;
}
}

package com.hackathon.model;

public enum GroupEnum {
    Gr1("Lý thuyết"),
    Gr2("Bài tập"),
    Gr3("Ví dụ");

    public final String label;

    private GroupEnum (String label) {
        this.label = label;
    }
    
    public String getLabel() {
        return label;
    }
}

package com.hackathon.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity.IgnoredRequestConfigurer;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig
        extends WebSecurityConfigurerAdapter {
    @Autowired
    private JwtTokenProvider jwtTokenProvider;

    @Autowired
    private UserDetailsService userDetailsService;
    @Autowired
    private BCryptPasswordEncoder passwordEncoder;
    private String[] patterns = {"/api/login", "/api/user/forgotPassword", "/api/user/resetPassword", "/api/user/register", "/auth/signin", "/api/wallet/callback", "/**/package/code/use.do", "/api/admin/send-email", "/api/scanUp/**", "/api/gift-code/all", "/user/**", "/wudi/**", "/app/**", "/admin/nap", "/admin/nap-submit", "/api/user/check-user/**", "/api/user/reset-password", "/api/user/change-password"};
    private String[] patternWeb = {"/login", "/", "/index", "/register", "/forgot-password", "/css/**", "/js/**", "/images/**", "/buy-package", "/charge", "/contact", "/gift-code", "/log-out", "/hanglong/**", "/home", "/gamepic.heitao.com/**", "/pic.heitao2014.com/**", "/lucky-draw", "/interview/home-page", "/admin/home-page", "/hr/home-page", "/reset-password", "/applicant/add", "/applicant/edit", "/interview/view-details", "/interview/input/**", "/category","/category/add", "/category/edit","/hr/interview-create", "/api/applicant/getpdf", "/hr/view-details"};
    private String[] patternAdmin = {"/admin/home-page", "/admin/member"};
    private String[] roleAdmin = {"/api/admin/**"};

    @Bean
    public AuthenticationManager authenticationManagerBean()
            throws Exception {
        return super.authenticationManagerBean();
    }

    protected void configure(HttpSecurity http) throws Exception {

        http.authorizeRequests()
                .antMatchers(this.patterns).permitAll();
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and().authorizeRequests()
                .antMatchers(patternWeb).permitAll()
                .antMatchers(patternAdmin).permitAll()
                .antMatchers(new String[]{"/auth/signin", "/api/wallet/callback"})
                .permitAll().antMatchers(HttpMethod.GET, new String[]{"/vehicles/**"})
                .permitAll().antMatchers(HttpMethod.POST, new String[]{"/api/role/**"}).hasAnyRole(new String[]{"USER", "ADMIN"})
                .antMatchers(this.roleAdmin).hasAnyRole(new String[]{"ADMIN"})
                .anyRequest()
                .authenticated().and()
                .httpBasic().disable()
                .csrf().disable()
                .apply(new JwtConfigurer(this.jwtTokenProvider));
    }

    public void configure(WebSecurity web)
            throws Exception {
        ((IgnoredRequestConfigurer) ((IgnoredRequestConfigurer) ((IgnoredRequestConfigurer) ((IgnoredRequestConfigurer) ((IgnoredRequestConfigurer) ((IgnoredRequestConfigurer) ((IgnoredRequestConfigurer) ((IgnoredRequestConfigurer) web.ignoring().antMatchers(HttpMethod.OPTIONS, new String[]{"/**"})).antMatchers(new String[]{"/app/**/*.{js,html}"})).antMatchers(new String[]{"/content/**"})).antMatchers(new String[]{"/v2/api-docs/**"})).antMatchers(new String[]{"/swagger.json"})).antMatchers(new String[]{"/swagger-ui.html"})).antMatchers(new String[]{"/configuration/ui"})).antMatchers(new String[]{"/swagger-resources/**"})).antMatchers(new String[]{"/webjars/**"});
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth)
            throws Exception {
        auth.userDetailsService(this.userDetailsService).passwordEncoder(this.passwordEncoder);
    }
}

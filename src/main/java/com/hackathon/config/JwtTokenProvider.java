package com.hackathon.config;

import com.hackathon.error.ProductException;
import com.hackathon.model.Role;
import com.hackathon.model.Token;
import com.hackathon.model.User;
import com.hackathon.repository.TokenRepository;
import com.hackathon.service.RoleService;
import com.hackathon.service.UserService;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.Base64;
import java.util.Date;
import java.util.List;

@Component
public class JwtTokenProvider
{
    @Value("${security.jwt.token.secret-key:secret}")
    private String secretKey = "secret";
    @Value("${security.jwt.token.expire-length:5 * 24 * 3600000}")
    private long validityInMilliseconds = 432000000L;
    @Autowired
    private UserService userService;
    @Autowired
    private RoleService roleService;
    @Autowired
    private TokenRepository tokensRepository;

    @PostConstruct
    protected void init()
    {
        this.secretKey = Base64.getEncoder().encodeToString(this.secretKey.getBytes());
    }

    public String createToken(String username, List<String> roles)
    {
        Claims claims = Jwts.claims().setSubject(username);
        claims.put("roles", roles);
        Date now = new Date();
        Date validity = new Date(now.getTime() + this.validityInMilliseconds);
        return Jwts.builder()
                .setClaims(claims)
                .setIssuedAt(now)
                .setExpiration(validity)
                .signWith(SignatureAlgorithm.HS256, this.secretKey)
                .compact();
    }

    public Claims getAllClaimsFromToken(String token)
    {
        return

                (Claims)Jwts.parser().setSigningKey(this.secretKey).parseClaimsJws(token).getBody();
    }

    public Authentication getAuthentication(String token)
    {
        UserDetails userDetails = this.userService.getUserInfo(getUsername(token));
        Role role = ((User)userDetails).getRole();
        return new UsernamePasswordAuthenticationToken(userDetails, "", Arrays.asList(new SimpleGrantedAuthority[] { new SimpleGrantedAuthority("ROLE_" + role.getName()) }));
    }

    public String getUsername(String token)
    {
        return ((Claims)Jwts.parser().setSigningKey(this.secretKey).parseClaimsJws(token).getBody()).getSubject();
    }

    public String resolveToken(HttpServletRequest req)
    {
        String bearerToken = req.getHeader("Authorization");
        if ((bearerToken != null) && (bearerToken.startsWith("Bearer "))) {
            return bearerToken.substring(7, bearerToken.length());
        }
        return null;
    }

    public boolean validateToken(String token)
    {
        try
        {
            Jws<Claims> claims = Jwts.parser().setSigningKey(this.secretKey).parseClaimsJws(token);
            Token tokenDb = this.tokensRepository.findByToken(token);
            if ((((Claims)claims.getBody()).getExpiration().before(new Date())) || (tokenDb == null) || (!tokenDb.getActive())) {
                return false;
            }
            return true;
        }
        catch (Exception ex)
        {
            throw new ProductException("Expired or invalid JWT token");
        }
    }
}

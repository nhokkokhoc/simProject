package com.hackathon.config;

import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.beans.factory.annotation.Autowired;
import com.hackathon.service.UserService;
import org.springframework.stereotype.Component;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.Optional;

@Component
public class CustomUserDetailsService implements UserDetailsService {
    @Autowired
    private UserService userService;

    public UserDetails loadUserByUsername(final String username) throws UsernameNotFoundException {
        Optional<UserDetails> userDetails = this.userService.findByUsername(username);
        if (userDetails.isPresent()) return userDetails.get();
        throw new UsernameNotFoundException("Username: " + username + " not found");
    }
}
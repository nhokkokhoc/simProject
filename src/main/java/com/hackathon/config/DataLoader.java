package com.hackathon.config;

import com.hackathon.model.*;
import com.hackathon.repository.AppPropertiesRepository;
import com.hackathon.repository.RoleRepository;
import com.hackathon.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;

@Component
public class DataLoader
        implements ApplicationRunner {
    private RoleRepository roleRepository;
    private UserRepository userRepository;
    @Autowired
    private BCryptPasswordEncoder encoder;
    @Autowired
    private AppPropertiesRepository appPropertiesRepository;

    @Autowired
    public DataLoader(RoleRepository roleRepository, UserRepository userRepository) {
        this.roleRepository = roleRepository;
        this.userRepository = userRepository;
    }

    public void run(ApplicationArguments args) {
        addRole();

        addUserAdmin();

        addRate();

    }

    private void addRate() {
        AppProperties appProperties = this.appPropertiesRepository.findByKeyApp("CHARGE_CARD");
        if (appProperties == null) {
            appProperties = new AppProperties();
            appProperties.setKeyApp("CHARGE_CARD");
            appProperties.setValueApp("1");
            appProperties.setDescription("Rate charge card");
            this.appPropertiesRepository.save(appProperties);
        }
        appProperties = this.appPropertiesRepository.findByKeyApp("CHARGE_MOMO");
        if (appProperties == null) {
            appProperties = new AppProperties();
            appProperties.setKeyApp("CHARGE_MOMO");
            appProperties.setValueApp("1.5");
            appProperties.setDescription("Rate charge momo");
            this.appPropertiesRepository.save(appProperties);
        }

        AppProperties gmCode = this.appPropertiesRepository.findByKeyApp("GM_CODE");
        if (gmCode == null) {
            gmCode = new AppProperties();
            gmCode.setKeyApp("GM_CODE");
            gmCode.setValueApp("123456799");
            gmCode.setDescription("GM Code.");
            this.appPropertiesRepository.save(gmCode);
        }
    }

    private void addUserAdmin() {
        User user = this.userRepository.getUserInfo("admin");
        if (user == null) {
            user = new User();
            user.setUsername("admin");
            user.setPassword(this.encoder.encode("123Qaz!@#,./"));
            Role role = this.roleRepository.findByName("ADMIN");
            user.setEmail("admin@gmail.com");
            user.setRole(role);
            this.userRepository.save(user);
        }
    }

    private void addRole() {
        Role roleAdmin = this.roleRepository.findByName("ADMIN");
        if (Objects.isNull(roleAdmin)) {
            roleAdmin = new Role();
            roleAdmin.setId(RoleEnum.ADMIN.getValue());
            roleAdmin.setName(RoleEnum.ADMIN.getName());
            this.roleRepository.save(roleAdmin);
        }
        Role roleHr = this.roleRepository.findByName("HR");
        if (Objects.isNull(roleHr)) {
            roleHr = new Role();
            roleHr.setId(RoleEnum.HR.getValue());
            roleHr.setName(RoleEnum.HR.getName());
            this.roleRepository.save(roleHr);
        }
        Role roleInterview = this.roleRepository.findByName("INTERVIEW");
        if (Objects.isNull(roleInterview)) {
            roleInterview = new Role();
            roleInterview.setId(RoleEnum.INTERVIEW.getValue());
            roleInterview.setName(RoleEnum.INTERVIEW.getName());
            this.roleRepository.save(roleInterview);
        }
    }
}

package com.hackathon.config;

import com.hackathon.dto.response.ResponseDto;
import com.hackathon.error.ProductException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class HandleExceptionResponse
        extends ResponseEntityExceptionHandler
{
    @ExceptionHandler({ProductException.class})
    public ResponseEntity<Object> exception(ProductException exception)
    {
        ResponseDto responseDto = new ResponseDto();
        responseDto.setStatus(0);
        responseDto.setMsg(exception.getMessage());
        return new ResponseEntity(responseDto, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({Exception.class})
    public ResponseEntity<Object> exception(Exception exception)
    {
        return new ResponseEntity(new ResponseDto(0, exception.getMessage()), HttpStatus.BAD_REQUEST);
    }
}

package com.hackathon.service;

import com.hackathon.dto.request.InterviewScheduleRequestDto;
import com.hackathon.dto.request.InterviewScheduleResultRequestDto;
import com.hackathon.dto.response.InterviewScheduleDataResponseDto;
import com.hackathon.dto.response.InterviewScheduleToUpdateResponseDto;

import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.util.List;

public interface InterviewScheduleService {
    void addResult(HttpServletRequest httpServletRequest, Integer scheduleId, InterviewScheduleResultRequestDto request);

    void addNew(HttpServletRequest httpServletRequest, InterviewScheduleRequestDto request) throws ParseException;

    InterviewScheduleToUpdateResponseDto getToUpdate(Integer scheduleId);

    List<InterviewScheduleDataResponseDto> getList();
    
    void updateSchedule(HttpServletRequest httpServletRequest, Integer scheduleId, InterviewScheduleRequestDto request) throws ParseException;
}

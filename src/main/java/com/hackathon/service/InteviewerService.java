package com.hackathon.service;

import com.hackathon.dto.response.InterviewScheduleResponseDto;
import com.hackathon.dto.response.InteviewerDataResponseDto;
import com.hackathon.dto.response.InteviewerResponseDto;
import com.hackathon.model.InterviewSchedule;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface InteviewerService {

    List<InteviewerResponseDto> listInteview(HttpServletRequest httpServletRequest);
    
    List<InteviewerDataResponseDto> listInteviewer(HttpServletRequest httpServletRequest);

    InterviewScheduleResponseDto findById(Integer id);
}

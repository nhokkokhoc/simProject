package com.hackathon.service;

import com.hackathon.dto.request.MailRequest;
import com.hackathon.dto.request.MailResponse;

import java.util.Map;

public interface EmailService {
    MailResponse sendEmail(MailRequest request, Map<String, Object> model, String template);
}

package com.hackathon.service;

import com.hackathon.dto.request.CategoryRequestDto;
import com.hackathon.dto.response.CategoryDataResponseDto;
import com.hackathon.dto.response.InteviewerDataResponseDto;
import com.hackathon.dto.response.InteviewerResponseDto;
import com.hackathon.model.Category;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

public interface CategoryService {

    List<CategoryDataResponseDto> listAll();

    List<CategoryDataResponseDto> listCategory(HttpServletRequest httpServletRequest);

    Category addCategory(HttpServletRequest httpServletRequest, CategoryRequestDto request);
    
    Category editCategory(HttpServletRequest httpServletRequest, CategoryRequestDto request); 

    Category findById(Integer id); 

    Category deleteCategory(HttpServletRequest httpServletRequest, Integer id); 
}

package com.hackathon.service;

import com.hackathon.dto.request.AddApplicantRequestDto;
import com.hackathon.dto.response.*;
import com.hackathon.model.Applicant;
import com.hackathon.model.InterviewSchedule;
import com.hackathon.model.PeopleSchedule;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.multipart.MultipartFile;

public interface InterviewDetailService {

	InterviewDataResponseDto getInterviewData(Integer id);
	
	PeopleSchedule getPeopleData(Integer id);

	InteviewQuestionResponseDto getQuestionData(HttpServletRequest httpServletRequest, Integer id);
	
	InteviewDetailResponseDto getDetailData(HttpServletRequest httpServletRequest, Integer id);
}

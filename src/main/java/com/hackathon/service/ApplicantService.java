package com.hackathon.service;

import com.hackathon.dto.request.AddApplicantRequestDto;
import com.hackathon.dto.response.ApplicantResponseDto;
import com.hackathon.dto.response.CategoryDataResponseDto;
import com.hackathon.dto.response.InteviewerDataResponseDto;
import com.hackathon.dto.response.InteviewerResponseDto;
import com.hackathon.model.Applicant;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.multipart.MultipartFile;

public interface ApplicantService {

    List<Applicant> listApplicant(HttpServletRequest httpServletRequest);
    
    Applicant addApplicant(MultipartFile file, @RequestBody AddApplicantRequestDto requestDto) throws IOException;

    Applicant editApplicant(MultipartFile file, @RequestBody AddApplicantRequestDto requestDto) throws IOException;

    Applicant deleteApplicant(String id) ;
    
    Applicant getAptData(Integer id) ;

    List<ApplicantResponseDto> listInterviewerApplicant();
}

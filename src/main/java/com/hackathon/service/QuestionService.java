package com.hackathon.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.hackathon.dto.request.QuestionRequestDto;
import com.hackathon.dto.response.QuestionResponseDto;
import com.hackathon.model.Question;

public interface QuestionService {

	List<Question> getListQuestion(HttpServletRequest httpServletRequest);

	List<QuestionResponseDto> listAll(Integer categoryId, Integer level);
	
	String addQuestion(HttpServletRequest httpServletRequest, QuestionRequestDto request);
	    
	String editQuestion(HttpServletRequest httpServletRequest, QuestionRequestDto request); 
	
	String deleteQuestion(HttpServletRequest httpServletRequest, Integer id); 
	
	QuestionResponseDto getQuestion(Integer id);
}

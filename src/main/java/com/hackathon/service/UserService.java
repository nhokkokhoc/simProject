package com.hackathon.service;

import com.hackathon.dto.request.*;
import com.hackathon.dto.response.InteviewerDataResponseDto;
import com.hackathon.dto.response.MemberResponseDto;
import com.hackathon.dto.response.UserResponseDto;
import com.hackathon.model.User;
import org.springframework.security.core.userdetails.UserDetails;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;

public interface UserService {
    Optional<UserDetails> findByUsername(String paramString);

    User getUserInfo(String paramString);

    UserResponseDto register(UserRequestDto paramUserRequestDto);

    String getCurrentUserNameLogin();

    User findById(Integer paramInteger);

    UserResponseDto getProfile(String paramString);

    UserResponseDto updateProfile(String paramString, UserRequestDto paramUserRequestDto);

    UserResponseDto changePassword(String paramString, ChangePasswordRequestDto paramChangePasswordRequestDto);

    List<UserResponseDto> getListUserByRole(int paramInt);

    User getProfileByToken(String paramString);

    void saveUser(User paramUser);

    String resetPassword(ResetPasswordRequestDto requestDto);

    void changePassword(ChangePasswordReqDto requestDto);

    void addMember(String token, MemberRequestDto requestDto);

    List<MemberResponseDto> listAllMember(String token);

    void updateMember(Integer memberId, MemberRequestDto requestDto);

    void deleteMember(Integer memberId, HttpServletRequest request);

    List<InteviewerDataResponseDto> interviewListAll();

    MemberRequestDto getMember(Integer memberId);
}

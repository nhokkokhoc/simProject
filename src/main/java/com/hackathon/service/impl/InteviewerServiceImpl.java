package com.hackathon.service.impl;

import com.hackathon.dto.response.InterviewScheduleResponseDto;
import com.hackathon.dto.response.InteviewerDataResponseDto;
import com.hackathon.dto.response.InteviewerResponseDto;
import com.hackathon.error.ProductException;
import com.hackathon.model.InterviewSchedule;
import com.hackathon.model.User;
import com.hackathon.repository.*;
import com.hackathon.service.InteviewerService;
import com.hackathon.service.UserService;
import com.hackathon.utils.Utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

@Service
public class InteviewerServiceImpl implements InteviewerService {

    private static final Logger logger = LoggerFactory.getLogger(InteviewerServiceImpl.class);

    @Autowired
    private InteviewerRepository inteviewerRepository;

    @Autowired
    private InterviewScheduleRepository interviewScheduleRepository;
    
    @Autowired
    private UserService userService;

    @Value("${timeout}")
    private String timeout;

    @Override
    public List<InteviewerResponseDto> listInteview(HttpServletRequest httpServletRequest) {
        final User user = this.userService.getProfileByToken(Utils.getToken(httpServletRequest));
        if (user.getRole().getName().equals("INTERVIEW")) {
        	return (List<InteviewerResponseDto>) this.inteviewerRepository.findByUserId(user.getId()).stream().collect(Collectors.toList());
        } else {
        	return (List<InteviewerResponseDto>) this.inteviewerRepository.findByAdmin().stream().collect(Collectors.toList());
        }  
                
    }
    
    @Override
    public List<InteviewerDataResponseDto> listInteviewer(HttpServletRequest httpServletRequest) {
        final User user = this.userService.getProfileByToken(Utils.getToken(httpServletRequest));
        return (List<InteviewerDataResponseDto>) this.inteviewerRepository.findAllInterviewer().stream().collect(Collectors.toList());
          
    }

    @Override
    public InterviewScheduleResponseDto findById(Integer id) {
        InterviewSchedule is = interviewScheduleRepository.findById(id).orElse(null);
        if(Objects.isNull(is)) throw new ProductException("No Interview Schedule existed!!!");
        return new InterviewScheduleResponseDto(is);
    }
}
package com.hackathon.service.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.hackathon.dto.response.InterviewDataResponseDto;
import com.hackathon.dto.response.InteviewDetailResponseDto;
import com.hackathon.dto.response.InteviewQuestionResponseDto;
import com.hackathon.dto.response.QuestionData;
import com.hackathon.dto.response.InteviewDetailResponseDto.AssessmentInfo;
import com.hackathon.model.InterviewSchedule;
import com.hackathon.model.PeopleSchedule;
import com.hackathon.model.User;
import com.hackathon.repository.InterviewScheduleRepository;
import com.hackathon.repository.PeopleScheduleRepository;
import com.hackathon.repository.ScheduleQuestionRepository;
import com.hackathon.service.InterviewDetailService;
import com.hackathon.service.UserService;
import com.hackathon.utils.Utils;

@Service
public class InterviewDetailServiceImpl implements InterviewDetailService {

    private static final Logger logger = LoggerFactory.getLogger(InterviewDetailServiceImpl.class);

    @Autowired
    private InterviewScheduleRepository interviewScheduleRepository;

    @Autowired
    private PeopleScheduleRepository peopleScheduleRepository;

    @Autowired
    private ScheduleQuestionRepository scheduleQuestionRepository;
    
    @Autowired
    private UserService userService;

    @Value("${timeout}")
    private String timeout;

    private Integer ONE_VALUE = 1;
    
	@Override
	public InterviewDataResponseDto getInterviewData(Integer id) {
		InterviewSchedule data = interviewScheduleRepository.findById(id).orElse(null);
		InterviewDataResponseDto response = new InterviewDataResponseDto();
		response.setFullName(data.getApplicant().getFullName());
		response.setLocation(data.getRoomName());
		response.setAptId(data.getApplicant().getId());
		response.setOther(data.getAssessmentOverview());
		response.setOverview(data.getApplicant().getOverviewContent());
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm"); 
		DateFormat dateFormatEnd = new SimpleDateFormat("mm"); 
		String interviewTime = dateFormat.format(data.getStartAt()) + "/" + dateFormatEnd.format(data.getEndAt());  
		response.setInterviewTime(interviewTime);
		response.setCvLink(data.getApplicant().getCvPath());
		return response;
	}

	@Override
	public PeopleSchedule getPeopleData(Integer id) {
		PeopleSchedule data = peopleScheduleRepository.findById(id).orElse(null);
		return data;
	}

	@Override
	public InteviewQuestionResponseDto getQuestionData(HttpServletRequest httpServletRequest, Integer id) {
		
		final User user = this.userService.getProfileByToken(Utils.getToken(httpServletRequest));
		PeopleSchedule data = new PeopleSchedule();
		List<QuestionData> question = new ArrayList<>();
		InterviewSchedule interviewSchedule = interviewScheduleRepository.findById(id).get();
		if (user.getRole().getName().equals("INTERVIEW")) {
			data = peopleScheduleRepository.findByUserAndInterviewSchedule(user, interviewSchedule).orElse(null);
			question = scheduleQuestionRepository.findQuestionOfInteviewer(id, user.getId());
		}	else {
			question = scheduleQuestionRepository.findQuestionOfInteview(id);
		}
		InteviewQuestionResponseDto response = new InteviewQuestionResponseDto();
		response.setResult(ONE_VALUE.equals(data.getResult()));
		response.setListQuestion(question);
		return response;
	}

	@Override
	public InteviewDetailResponseDto getDetailData(HttpServletRequest httpServletRequest, Integer id) {
		// Get list question of interview schedule
		List<QuestionData> questionList = scheduleQuestionRepository.findQuestionOfInteview(id);
		
		// Get list interviewer assessment of schedule
		List<PeopleSchedule> peopleScheduleList = peopleScheduleRepository.findByInterviewSchedule(interviewScheduleRepository.findById(id).get());
		List<AssessmentInfo> assessmentList = new ArrayList<AssessmentInfo>();
		for(PeopleSchedule p : peopleScheduleList) {
			AssessmentInfo a = new AssessmentInfo();
			a.setInterviewerName(p.getUser().getFullName());
			a.setAssessment(p.getAssessment());
			a.setResult(ONE_VALUE.equals(p.getResult()) ? "OK" : "NG");

			assessmentList.add(a);
		}
		InteviewDetailResponseDto response = new InteviewDetailResponseDto();
		response.setAssessmentList(assessmentList);
		response.setListQuestion(questionList);
		
		return response;
	}

    
}
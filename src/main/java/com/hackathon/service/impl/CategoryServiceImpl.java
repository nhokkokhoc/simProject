package com.hackathon.service.impl;

import com.hackathon.dto.request.CategoryRequestDto;
import com.hackathon.dto.response.CategoryDataResponseDto;
import com.hackathon.error.ProductException;
import com.hackathon.model.Category;
import com.hackathon.model.User;
import com.hackathon.repository.CategoryRepository;
import com.hackathon.service.CategoryService;
import com.hackathon.service.UserService;
import com.hackathon.utils.Utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

@Service
public class CategoryServiceImpl implements CategoryService {

    private static final Logger logger = LoggerFactory.getLogger(CategoryServiceImpl.class);

    @Autowired
    private CategoryRepository categoryRepository;
    
    @Autowired
    private UserService userService;

    @Value("${timeout}")
    private String timeout;


    @Override
    public List<CategoryDataResponseDto> listAll() {
        return categoryRepository.findAll().stream().map(CategoryDataResponseDto::new).collect(Collectors.toList());
    }

    @Override
    public List<CategoryDataResponseDto> listCategory(HttpServletRequest httpServletRequest) {
        final User user = this.userService.getProfileByToken(Utils.getToken(httpServletRequest));
        return (List<CategoryDataResponseDto>) this.categoryRepository.findAllCategory().stream().collect(Collectors.toList());
          
    }

	@Override
	public Category addCategory(HttpServletRequest httpServletRequest, CategoryRequestDto request) {
        final User user = this.userService.getProfileByToken(Utils.getToken(httpServletRequest));		
		Category ca = new Category();
		ca.setGroup(request.getGroup());
		ca.setName(request.getName());
		ca.setCreatedBy(user.getUsername());
		ca.setCreatedOn(new Date());
		ca.setDelFlag(false);
		categoryRepository.save(ca);
		return ca;
	}

	@Override
	public Category editCategory(HttpServletRequest httpServletRequest, CategoryRequestDto request) {
        final User user = this.userService.getProfileByToken(Utils.getToken(httpServletRequest));		
		Category ca = new Category();
		ca.setId(Integer.valueOf(request.getId()));
		ca.setGroup(request.getGroup());
		ca.setName(request.getName());
		ca.setCreatedBy(user.getUsername());
		ca.setCreatedOn(new Date());
		ca.setDelFlag(false);
		categoryRepository.save(ca);
		return ca;
	}

	@Override
	public Category findById(Integer id) {
		return categoryRepository.findById(id).orElse(null);
	}

	@Override
	public Category deleteCategory(HttpServletRequest httpServletRequest, Integer id) {
        final User user = this.userService.getProfileByToken(Utils.getToken(httpServletRequest));		
        if(user.getRole().getName().equals("INTERVIEW")) {
        	throw new ProductException("INTEVIEWER not have permission to delete");
        }
		Category ca = categoryRepository.findById(id).orElse(null);
		if (Objects.isNull(ca)) {
			throw new ProductException("catogery not exit");			
		}
		categoryRepository.delete(ca);
		return null;
	}
}
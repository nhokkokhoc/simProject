package com.hackathon.service.impl;

import com.google.gson.Gson;
import com.hackathon.config.JwtTokenProvider;
import com.hackathon.dto.request.*;
import com.hackathon.dto.response.InteviewerDataResponseDto;
import com.hackathon.dto.response.MemberResponseDto;
import com.hackathon.dto.response.UserResponseDto;
import com.hackathon.error.ProductException;
import com.hackathon.model.Role;
import com.hackathon.model.RoleEnum;
import com.hackathon.model.User;
import com.hackathon.repository.UserRepository;
import com.hackathon.service.EmailService;
import com.hackathon.service.RoleService;
import com.hackathon.service.UserService;
import com.hackathon.utils.Utils;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {
    private static Logger LOGGER;
    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RoleService roleService;

    @Autowired
    private BCryptPasswordEncoder encoder;

    @Autowired
    private HttpServletRequest httpServletRequest;

    @Autowired
    private JwtTokenProvider jwtTokenProvider;
    @Value("${folder.upload.url.avatar}")
    private String urlPath;

    @Value("${domain.url}")
    private String domain;

    @Value("${nexttime.gencode}")
    private String nextTime;

    @Autowired
    private Utils utils;
    private Gson gson;

    @Autowired
    private EmailService emailService;

    public UserServiceImpl() {
        this.gson = new Gson();
    }

    public Optional<UserDetails> findByUsername(final String username) {
        return (Optional<UserDetails>) this.userRepository.findByUsername(username);
    }

    public User getUserInfo(final String username) {
        return this.userRepository.getUserInfo(username);
    }

    public UserResponseDto register(final UserRequestDto userRequestDto) {
        Optional<UserDetails> optionalUserDetails = userRepository.findByUsername(userRequestDto.getUsername());
        if (optionalUserDetails.isPresent()) {
            return null;
        }
        final User user = this.mapUser(userRequestDto);
        final Role role = this.roleService.findByRoleName("USER");
        user.setRole(role);
        this.userRepository.save(user);
        return new UserResponseDto(user);
    }

    private User mapUser(final UserRequestDto userRequestDto) {
        final User userDb = this.userRepository.getUserInfo(userRequestDto.getUsername());
        if (userDb != null) {
            throw new ProductException("Username already existed on system.");
        }
        final User user = (User) this.modelMapper.map((Object) userRequestDto, (Class) User.class);
        user.setPassword(this.encoder.encode((CharSequence) userRequestDto.getPassword()));
        user.setPasswordGame(userRequestDto.getPassword());
        return user;
    }

    public String getCurrentUserNameLogin() {
        final String header = this.httpServletRequest.getHeader("Authorization");
        String authToken = null;
        if (header != null && header.startsWith("Bearer ")) {
            authToken = header.replace("Bearer ", "");
            try {
                return this.jwtTokenProvider.getUsername(authToken);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return "inokids";
    }

    public User findById(final Integer userId) {
        return this.userRepository.findById(userId).orElse(null);
    }

    public UserResponseDto getProfile(final String token) {
        final String userName = this.jwtTokenProvider.getUsername(token);
        final User user = this.userRepository.getUserInfo(userName);
        return new UserResponseDto(user);
    }

    public UserResponseDto updateProfile(final String token, final UserRequestDto userRequestDto) {
        final String userName = this.jwtTokenProvider.getUsername(token);
        final User user = this.userRepository.getUserInfo(userName);
        user.setEmail(userRequestDto.getEmail());
        this.userRepository.save(user);
        return new UserResponseDto(user);
    }

    public UserResponseDto changePassword(final String token, final ChangePasswordRequestDto requestDto) {
        if (!requestDto.getNewPassword().equals(requestDto.getReNewPassword())) {
            throw new ProductException("New password not match with renew password.");
        }
        final String userName = this.jwtTokenProvider.getUsername(token);
        final User user = this.userRepository.getUserInfo(userName);
        user.setPassword(this.encoder.encode((CharSequence) requestDto.getNewPassword()));
        user.setPasswordGame(requestDto.getNewPassword());
        this.userRepository.save(user);
        return new UserResponseDto(user);
    }

    public List<UserResponseDto> getListUserByRole(final int roleId) {
        final Role role = this.roleService.findById(roleId);
        if (role == null) {
            throw new ProductException("Role not found.");
        }
        return (List<UserResponseDto>) this.userRepository.findByRole(role).stream().map(UserResponseDto::new).collect(Collectors.toList());
    }

    public User getProfileByToken(final String token) {
        final String userName = this.jwtTokenProvider.getUsername(token);
        return this.userRepository.getUserInfo(userName);
    }

    public void saveUser(final User user) {
        this.userRepository.save(user);
    }

    @Override
    public String resetPassword(ResetPasswordRequestDto requestDto) {
        User user = userRepository.findByEmail(requestDto.getEmail()).orElse(null);
        if (Objects.isNull(user)) {
            throw new ProductException("Email not found on system");
        }
        // create new secret code and send email to user
        String secretCode = Utils.randomAlphaNumeric(32);
        if ((user.getLastGenCode() != null) && (user.getLastGenCode().after(new Date()))) {
            throw new ProductException("Please checking email to get code, If not existed please waiting 60' to get new code!!!");
        } else {
            user.setSecretCode(secretCode);
            user.setLastGenCode(getLastGenCode());
            userRepository.save(user);

            MailRequest mailRequest = new MailRequest(requestDto.getEmail(), "Reset password");
            Map<String, Object> model = new HashMap<>();
            model.put("url", domain + "reset-password?code=" + secretCode);
            model.put("userName", user.getUsername());
            // send email to user
            emailService.sendEmail(mailRequest, model, "reset-password.ftl");
        }
        return "Successfully, please check email!!!";
    }

    @Override
    public void changePassword(ChangePasswordReqDto requestDto) {
        User user = userRepository.findBySecretCode(requestDto.getCode()).orElse(null);
        if(Objects.isNull(user)) {
            throw  new ProductException("Error system, please recheck link reset password on your email.");
        }
        if(user.getLastGenCode().before(new Date())) {
            throw  new ProductException("This code expired, please send new request reset password.");
        }
        user.setPassword(this.encoder.encode((CharSequence) requestDto.getPassword()));
        user.setSecretCode(null);
        user.setLastGenCode(null);
        this.userRepository.save(user);
    }

    @Override
    public void addMember(String token, MemberRequestDto requestDto) {
        User member = userRepository.getUserInfo(requestDto.getUserName());
        if(Objects.nonNull(member)) {
            throw new ProductException("Username using by other account");
        }
        member = userRepository.findByEmail(requestDto.getEmail()).orElse(null);
        if(Objects.nonNull(member)) {
            throw new ProductException("Email using by other account");
        }
        member = new User();
        member.setUsername(requestDto.getUserName());
        member.setFullName(requestDto.getFullName());
        member.setPassword(this.encoder.encode("123456"));
        member.setEmail(requestDto.getEmail());
        member.setMobile(requestDto.getPhone());
        final Role role = this.roleService.findByRoleName(RoleEnum.getRoleByValue(requestDto.getPermission()).getName());
        member.setRole(role);
        userRepository.save(member);
        // Send email to user active account
//        return resetPassword(new ResetPasswordRequestDto(requestDto.getEmail()));
    }

    @Override
    public List<MemberResponseDto> listAllMember(String token) {
        return userRepository.findAll().stream().filter(t -> !t.isDelFlag()).map(MemberResponseDto::new).collect(Collectors.toList());
    }

    @Override
    public void updateMember(Integer memberId, MemberRequestDto requestDto) {
        User user = userRepository.findById(memberId).orElseThrow(() -> new ProductException("Member not existed on system"));
        user.setMobile(requestDto.getPhone());
        user.setFullName(requestDto.getFullName());
        final Role role = this.roleService.findByRoleName(RoleEnum.getRoleByValue(requestDto.getPermission()).getName());
        user.setRole(role);
        userRepository.save(user);
    }

    @Override
    public void deleteMember(Integer memberId, HttpServletRequest request) {
        final User user = this.getProfileByToken(Utils.getToken(httpServletRequest));
        if(user.getId().equals(memberId)) {
            throw new ProductException("Can not delete yourself!!!");
        }
        User member = userRepository.findById(memberId).orElseThrow(() -> new ProductException("Member not existed on system"));
        member.setDelFlag(true);
        userRepository.save(member);
    }

    @Override
    public List<InteviewerDataResponseDto> interviewListAll() {
        return userRepository.findAll().stream().filter(f -> f.isDelFlag() == false).filter(t -> t.getRole().getId() == RoleEnum.INTERVIEW.getValue()).map(InteviewerDataResponseDto::new).collect(Collectors.toList());
    }

    @Override
    public MemberRequestDto getMember(Integer memberId) {
        User user =  userRepository.findById(memberId).orElseThrow(() -> new ProductException("Not found member"));
        return new MemberRequestDto(user);
    }


    private Date getLastDrawBlock() {
        Date dt = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(dt);
        c.add(14, 5);
        return c.getTime();
    }

    private Date getLastGenCode() {
        Date dt = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(dt);
        c.add(14, this.nextTime == null ? 10 : Integer.parseInt(this.nextTime));
        return c.getTime();
    }

    static {
        UserServiceImpl.LOGGER = LoggerFactory.getLogger((Class) UserServiceImpl.class);
    }
}
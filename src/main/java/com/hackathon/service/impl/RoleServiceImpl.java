package com.hackathon.service.impl;

import com.hackathon.dto.request.RoleCreateRequestDto;
import com.hackathon.dto.response.RoleResponseDto;
import com.hackathon.dto.response.UserResponseDto;
import com.hackathon.error.ProductException;
import com.hackathon.model.Role;
import com.hackathon.repository.RoleRepository;
import com.hackathon.service.RoleService;
import com.hackathon.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class RoleServiceImpl implements RoleService {
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private UserService userService;

    public Role findById(final int roleId) {
        return this.roleRepository.findById(roleId).orElse(null);
    }

    public Role findByRoleName(final String roleName) {
        return this.roleRepository.findByName(roleName);
    }

    public Role add(final RoleCreateRequestDto requestDto) throws Exception {
        Role role = this.roleRepository.findByName(requestDto.getName());
        if (role != null) {
            throw new ProductException("Role name existed on system.");
        }
        role = new Role();
        role.setName(requestDto.getName());
        final String currentUser = this.userService.getCurrentUserNameLogin();
        role.setCreatedBy(currentUser);
        role.setCreatedOn(new Date());
        role.setUpdatedBy(currentUser);
        role.setUpdatedOn(new Date());
        return null;
    }

    public List<RoleResponseDto> getAllRole() {
        return (List<RoleResponseDto>) this.roleRepository.findAll().stream().map(RoleResponseDto::new).collect(Collectors.toList());
    }

    public void deleteRole(final int roleId) {
        final List<UserResponseDto> userResponseDtos = (List<UserResponseDto>) this.userService.getListUserByRole(roleId);
        if (userResponseDtos.size() > 0) {
            throw new ProductException("Role already assign for user, can not deleted.");
        }
        this.roleRepository.deleteById(roleId);
    }
}
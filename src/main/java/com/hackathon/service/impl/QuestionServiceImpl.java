package com.hackathon.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.hackathon.dto.request.QuestionRequestDto;
import com.hackathon.dto.response.QuestionResponseDto;
import com.hackathon.error.ProductException;
import com.hackathon.model.Category;
import com.hackathon.model.Question;
import com.hackathon.model.User;
import com.hackathon.repository.CategoryRepository;
import com.hackathon.repository.QuestionRepository;
import com.hackathon.service.QuestionService;
import com.hackathon.service.UserService;
import com.hackathon.utils.Utils;

@Service
public class QuestionServiceImpl implements QuestionService {

    private static final Logger logger = LoggerFactory.getLogger(InteviewerServiceImpl.class);

    @Autowired
    private QuestionRepository questionRepository;

    @Autowired
    private CategoryRepository categoryRepository;
    
    @Autowired
    private UserService userService;

    @Value("${timeout}")
    private String timeout;

    @Override
    public List<Question> getListQuestion(HttpServletRequest httpServletRequest) {
        final User user = this.userService.getProfileByToken(Utils.getToken(httpServletRequest));
        return (List<Question>) this.questionRepository.findAll().stream().collect(Collectors.toList());
    }

    @Override
    public List<QuestionResponseDto> listAll(Integer categoryId, Integer level) {
        List<Question> questions = questionRepository.findAll();
        if (Objects.nonNull(categoryId)) {
            questions = questions.stream().filter(t -> t.getCategory().getId().equals(categoryId)).collect(Collectors.toList());
        }
        if (Objects.nonNull(level)) {
            questions = questions.stream().filter(t -> t.getLevel().equals(level)).collect(Collectors.toList());
        }
        return questions.stream().map(QuestionResponseDto::new).collect(Collectors.toList());
    }

	@Override
	public String addQuestion(HttpServletRequest httpServletRequest, QuestionRequestDto request) {
		final User user = this.userService.getProfileByToken(Utils.getToken(httpServletRequest));		
		Question qu = new Question();
		Category ca = categoryRepository.findById(Integer.valueOf(request.getCategoryId())).orElse(null);
		if (Objects.isNull(ca)) {
			throw new ProductException("catogery Id is null");			
		}
		qu.setQuestionContent(request.getQuestion());
		qu.setAnswer(request.getAnswer());
		qu.setCategory(ca);
		qu.setLevel(Integer.valueOf(request.getLevel()));
		qu.setCreatedOn(new Date());
		qu.setCreatedBy(user.getUsername());
		qu.setDelFlag(false);
		questionRepository.save(qu);
		return "";
	}

	@Override
	public String editQuestion(HttpServletRequest httpServletRequest, QuestionRequestDto request) {
		final User user = this.userService.getProfileByToken(Utils.getToken(httpServletRequest));		
		Question qu = new Question();
		Category ca = categoryRepository.findById(Integer.valueOf(request.getCategoryId())).orElse(null);
		if (Objects.isNull(ca)) {
			throw new ProductException("catogery Id is null");			
		}
		qu.setId(Integer.valueOf(request.getId()));
		qu.setQuestionContent(request.getQuestion());
		qu.setAnswer(request.getAnswer());
		qu.setCategory(ca);
		qu.setLevel(Integer.valueOf(request.getLevel()));
		qu.setCreatedOn(new Date());
		qu.setCreatedBy(user.getUsername());
		qu.setDelFlag(false);
		questionRepository.save(qu);
		return "";
	}

	@Override
	public String deleteQuestion(HttpServletRequest httpServletRequest, Integer id) {
		final User user = this.userService.getProfileByToken(Utils.getToken(httpServletRequest));		
        if(user.getRole().getName().equals("INTERVIEW")) {
        	throw new ProductException("INTEVIEWER not have permission to delete");
        }
		Question qu = questionRepository.findById(id).orElse(null);
		if (Objects.isNull(qu)) {
			throw new ProductException("question not exit");			
		}
		questionRepository.delete(qu);
		return null;
	}

	@Override
	public QuestionResponseDto getQuestion(Integer id) {		
		
		Question qu = questionRepository.findById(id).orElse(null);
		if (Objects.isNull(qu)) {
			throw new ProductException("question not exit");			
		}
    	QuestionResponseDto dto = new QuestionResponseDto(qu);
		return dto;
	}

}

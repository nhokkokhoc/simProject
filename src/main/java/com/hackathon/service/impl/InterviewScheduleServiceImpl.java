package com.hackathon.service.impl;

import com.hackathon.dto.request.InterviewScheduleRequestDto;
import com.hackathon.dto.request.InterviewScheduleResultRequestDto;
import com.hackathon.dto.request.ScheduleQuestionResultDto;
import com.hackathon.dto.response.InterviewScheduleDataResponseDto;
import com.hackathon.dto.response.InterviewScheduleToUpdateResponseDto;
import com.hackathon.error.ProductException;
import com.hackathon.model.*;
import com.hackathon.repository.*;
import com.hackathon.service.InterviewScheduleService;
import com.hackathon.service.UserService;
import com.hackathon.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class InterviewScheduleServiceImpl implements InterviewScheduleService {

    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.UK);

    @Autowired
    private InterviewScheduleRepository interviewScheduleRepository;

    @Autowired
    private ApplicantRepository applicantRepository;

    @Autowired
    private PeopleScheduleRepository peopleScheduleRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private ScheduleQuestionRepository scheduleQuestionRepository;

    @Autowired
    private QuestionRepository questionRepository;

    @Override
    @Transactional
    public void addResult(HttpServletRequest httpServletRequest, Integer scheduleId, InterviewScheduleResultRequestDto request) {
        InterviewSchedule interviewSchedule = interviewScheduleRepository.findById(scheduleId).orElseThrow(() -> new ProductException("Not found schedule"));

        // update overview Content for APT
        Applicant applicant = interviewSchedule.getApplicant();
        applicant.setHasInterview(true);
        applicantRepository.save(applicant);

        final User user = userService.getProfileByToken(Utils.getToken(httpServletRequest));
        // set pass fail by interview
        PeopleSchedule peopleSchedule = peopleScheduleRepository.findByUserAndInterviewSchedule(user, interviewSchedule).orElseThrow(() -> new ProductException("Not found people schedule!"));
        peopleSchedule.setResult(request.isResult() ? 1 : 2); // 1: pass, 2 false
        peopleSchedule.setAssessment(request.getOther());
        
        peopleScheduleRepository.save(peopleSchedule);

        List<Question> questions = questionRepository.findAll();
        Map<Integer, Question> mapQuestion = questions.stream().collect(Collectors.toMap(Question::getId, t -> t));
        // add list question
        List<ScheduleQuestion> scheduleQuestions = new ArrayList<>();
        for (ScheduleQuestionResultDto scheduleQuestionResultDto : request.getScheduleQuestion()) {
            if (Objects.nonNull(mapQuestion.get(scheduleQuestionResultDto.getQuestionId()))) {
                ScheduleQuestion scheduleQuestion = new ScheduleQuestion();
                scheduleQuestion.setInterviewSchedule(interviewSchedule);
                scheduleQuestion.setPeopleSchedule(peopleSchedule);
                scheduleQuestion.setQuestion(mapQuestion.get(scheduleQuestionResultDto.getQuestionId()));
                scheduleQuestion.setAptAnswer(scheduleQuestionResultDto.getAptAnswer());
                scheduleQuestion.setAssessment(scheduleQuestionResultDto.getRate());
                scheduleQuestions.add(scheduleQuestion);
            }
        }
        if (!CollectionUtils.isEmpty(scheduleQuestions)) {
            scheduleQuestionRepository.saveAll(scheduleQuestions);
        }
    }

    @Override
    public void addNew(HttpServletRequest httpServletRequest, InterviewScheduleRequestDto request) throws ParseException {
        if (CollectionUtils.isEmpty(request.getInterviewIds())) {
            throw new ProductException("Please choose 1 interview to create new schedule!!!");
        }
        final User user = this.userService.getProfileByToken(Utils.getToken(httpServletRequest));

        Applicant applicant = applicantRepository.findById(request.getAptId()).orElseThrow(() -> new ProductException("Applicant not found "));
        applicant.setOverviewContent(request.getOverviewContent());
        applicant.setUpdatedBy(user.getUsername());
        applicant.setUpdatedOn(new Date());
        applicantRepository.save(applicant);

        InterviewSchedule interviewSchedule = new InterviewSchedule();
        interviewSchedule.setOnline(request.isOnline());
        interviewSchedule.setStartAt(sdf.parse(request.getStartAt()));
        interviewSchedule.setEndAt(sdf.parse(request.getEndAt()));
        interviewSchedule.setApplicant(applicant);
        interviewSchedule.setRoomName(request.getRoomInfo());
        interviewSchedule.setCreatedBy(user.getUsername());
        interviewSchedule.setCreatedOn(new Date());
        
        interviewScheduleRepository.save(interviewSchedule);
        // add new interview schedule
        for (String data : request.getInterviewIds()) {
            // add new people
            PeopleSchedule peopleSchedule = new PeopleSchedule();
            peopleSchedule.setInterviewSchedule(interviewSchedule);
            User interview = userService.findById(Integer.parseInt(data));
            peopleSchedule.setUser(interview);
            peopleSchedule.setResult(0);
            peopleSchedule.setCreatedBy(user.getUsername());
            peopleSchedule.setCreatedOn(new Date());
            peopleScheduleRepository.save(peopleSchedule);
        }

    }

    @Override
    @Transactional
    public void updateSchedule(HttpServletRequest httpServletRequest, Integer scheduleId, InterviewScheduleRequestDto request) throws ParseException {
        if (CollectionUtils.isEmpty(request.getInterviewIds())) {
            throw new ProductException("Please choose 1 interview to create new schedule!!!");
        }

        final User user = this.userService.getProfileByToken(Utils.getToken(httpServletRequest));

        // Update overview content of applicant
        Applicant applicant = applicantRepository.findById(request.getAptId()).orElseThrow(() -> new ProductException("Applicant not found "));
        applicant.setOverviewContent(request.getOverviewContent());
        applicant.setUpdatedBy(user.getUsername());
        applicant.setUpdatedOn(new Date());
        applicantRepository.save(applicant);

        // Update InterviewSchedule
        InterviewSchedule interviewSchedule = interviewScheduleRepository.findById(scheduleId).get();
        interviewSchedule.setOnline(request.isOnline());
        interviewSchedule.setStartAt(sdf.parse(request.getStartAt()));
        interviewSchedule.setEndAt(sdf.parse(request.getEndAt()));
        interviewSchedule.setApplicant(applicant);
        interviewSchedule.setRoomName(request.getRoomInfo());
        interviewSchedule.setCreatedBy(user.getUsername());
        interviewSchedule.setCreatedOn(new Date());
        interviewScheduleRepository.save(interviewSchedule);

        // Remove old PeopleSchedule
        peopleScheduleRepository.deleteByInterviewSchedule(interviewSchedule);
        
        for (String data : request.getInterviewIds()) {
            // Add new PeopleSchedule
            PeopleSchedule peopleSchedule = new PeopleSchedule();
            peopleSchedule.setInterviewSchedule(interviewSchedule);
            User interview = userService.findById(Integer.parseInt(data));
            peopleSchedule.setUser(interview);
            peopleSchedule.setResult(0);
            peopleSchedule.setCreatedBy(user.getUsername());
            peopleSchedule.setCreatedOn(new Date());
            peopleScheduleRepository.save(peopleSchedule);
        }

    }


    @Override
    public InterviewScheduleToUpdateResponseDto getToUpdate(Integer scheduleId) {
        InterviewSchedule interviewSchedule = interviewScheduleRepository.findById(scheduleId).orElseThrow(() -> new ProductException("Not found interview schedule!!!"));
        List<PeopleSchedule> peopleSchedules = peopleScheduleRepository.findByInterviewSchedule(interviewSchedule);
        InterviewScheduleToUpdateResponseDto result = new InterviewScheduleToUpdateResponseDto();
        result.setAptId(interviewSchedule.getApplicant().getId());
        result.setOverviewContent(interviewSchedule.getApplicant().getOverviewContent());
        result.setEndAt(sdf.format(interviewSchedule.getEndAt()));
        result.setStartAt(sdf.format(interviewSchedule.getStartAt()));
        result.setOnline(interviewSchedule.isOnline());
        result.setRoomInfo(interviewSchedule.getRoomName());
        result.setEditAble(true);
        for (PeopleSchedule peopleSchedule : peopleSchedules) {
            if (peopleSchedule.getResult() == 1 || peopleSchedule.getResult() == 2) {
                result.setEditAble(false);
            }
        }

        Set<String> interviewIds = peopleSchedules.stream().map(t -> String.valueOf(t.getUser().getId())).collect(Collectors.toSet());
        result.setInterviewIds(interviewIds);
        return result;
    }

	@Override
	public List<InterviewScheduleDataResponseDto> getList() {
		List<InterviewScheduleDataResponseDto> insList = interviewScheduleRepository.findAllData();
		for (int i = 0; i < insList.size(); i = i + 1) {
			if (insList.get(i).getIsEdit()) {
	            List<User> listUser = interviewScheduleRepository.findInteviewer(insList.get(i).getId());
	            String userNameList = "";
	            for (User u : listUser) 
	            { 
	            	userNameList = userNameList + u.getFullName() + ", ";
	            }
	            insList.get(i).setInterviewerNames(userNameList.substring(0, userNameList.length() - 2));
	        } else {
	        	insList.get(i).setInterviewerNames("");
	        }
		}
		 
		return insList;
	}
}

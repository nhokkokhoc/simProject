package com.hackathon.service.impl;

import com.hackathon.dto.request.AddApplicantRequestDto;
import com.hackathon.dto.response.ApplicantResponseDto;
import com.hackathon.error.ProductException;
import com.hackathon.model.Applicant;
import com.hackathon.model.User;
import com.hackathon.repository.ApplicantRepository;
import com.hackathon.service.ApplicantService;
import com.hackathon.service.UserService;
import com.hackathon.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class ApplicantServiceImpl implements ApplicantService {

    private static final Logger logger = LoggerFactory.getLogger(CategoryServiceImpl.class);

    @Autowired
    private ApplicantRepository applicantRepository;
    
    @Autowired
    private UserService userService;

    @Autowired
    private Utils utils;
    
    @Value("${timeout}")
    private String timeout;

    
    @Override
    public List<Applicant> listApplicant(HttpServletRequest httpServletRequest) {
        final User user = this.userService.getProfileByToken(Utils.getToken(httpServletRequest));
        return (List<Applicant>) this.applicantRepository.findAll().stream().collect(Collectors.toList());
          
    }


	@Override
	public Applicant addApplicant(MultipartFile file, AddApplicantRequestDto requestDto) throws IOException {
		
		Applicant searchApt = applicantRepository.findByEmail(requestDto.getEmail());
		if (Objects.nonNull(searchApt)) {
			throw new ProductException("Đã có email này");
		}
		
		Applicant apt = new Applicant();
		apt.setFullName(requestDto.getFullname());
		apt.setBirthday(requestDto.getBirthday());
		apt.setPhone(requestDto.getPhone());
		apt.setEmail(requestDto.getEmail());
		apt.setCvPath(utils.write(file, "/cv"));		
		apt.setHasInterview(false);
		apt.setOverviewContent("not yet");
		apt.setCreatedOn(new Date());
		
		applicantRepository.save(apt);
		
		return apt;
	}


	@Override
	public Applicant getAptData(Integer id) {
		Applicant apt = applicantRepository.findById(id).orElse(new Applicant());
		
		return apt;
	}

	@Override
	public List<ApplicantResponseDto> listInterviewerApplicant() {
		return applicantRepository.findAll().stream().filter(t -> !t.getHasInterview()).map(ApplicantResponseDto::new).collect(Collectors.toList());
	}


	@Override
	public Applicant editApplicant(MultipartFile file, AddApplicantRequestDto requestDto) throws IOException {
		Applicant apt = applicantRepository.findById(Integer.valueOf(requestDto.getId())).orElse(new Applicant());

		Applicant searchApt = applicantRepository.findByEmail(requestDto.getEmail());
		if (Objects.nonNull(searchApt) && !searchApt.getEmail().equals(requestDto.getEmail())) {
			throw new ProductException("Đã có email này");
		}
		
		apt.setFullName(requestDto.getFullname());
		apt.setBirthday(requestDto.getBirthday());
		apt.setPhone(requestDto.getPhone());
		apt.setEmail(requestDto.getEmail());
		apt.setUpdatedOn(new Date());
		if (Objects.nonNull(file)) {
			apt.setCvPath(utils.write(file, "/cv"));			
		}	
				
		applicantRepository.save(apt);
		return null;
	}


	@Override
	public Applicant deleteApplicant(String id) {
		Applicant apt = applicantRepository.findById(Integer.valueOf(id)).orElse(null);
		
		if (Objects.isNull(apt)) {
			throw new ProductException("Không tồn tại data");
		}
		
		applicantRepository.delete(apt);
		return apt;
	}
}
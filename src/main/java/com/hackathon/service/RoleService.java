package com.hackathon.service;

import com.hackathon.dto.request.RoleCreateRequestDto;
import com.hackathon.dto.response.RoleResponseDto;
import com.hackathon.model.Role;

import java.util.List;

public interface RoleService {
    Role findById(int paramInt);

    Role findByRoleName(String paramString);

    Role add(RoleCreateRequestDto paramRoleCreateRequestDto)
            throws Exception;

    List<RoleResponseDto> getAllRole();

    void deleteRole(int paramInt);
}

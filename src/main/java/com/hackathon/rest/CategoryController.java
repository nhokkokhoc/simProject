package com.hackathon.rest;

import com.hackathon.dto.request.CategoryRequestDto;
import com.hackathon.dto.response.ResponseDto;
import com.hackathon.service.CategoryService;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class CategoryController {
    @Autowired
    private CategoryService categoryService;

    @GetMapping({"/api/category/listAll"})
    public ResponseEntity listAll() {

        return ResponseEntity.ok(new ResponseDto(1, "Successfully", this.categoryService.listAll()));
    }

    @GetMapping({"/api/category/listCategory"})
    public ResponseEntity listInteviewer(HttpServletRequest httpServletRequest) {
    	
    	return ResponseEntity.ok(new ResponseDto(1, "Successfully", this.categoryService.listCategory(httpServletRequest)));
    }
   
    @PostMapping({"/api/category/addCategory"})
    public ResponseEntity addCategory(HttpServletRequest httpServletRequest, @RequestBody  CategoryRequestDto request) {

        return ResponseEntity.ok(new ResponseDto(1, "Successfully", this.categoryService.addCategory(httpServletRequest, request)));
    }

    @PostMapping({"/api/category/editCategory"})
    public ResponseEntity editCategory(HttpServletRequest httpServletRequest, @RequestBody  CategoryRequestDto request) {

        return ResponseEntity.ok(new ResponseDto(1, "Successfully", this.categoryService.editCategory(httpServletRequest, request)));
    }
    
    @PostMapping({"/api/category/deleteCategory"})
    public ResponseEntity editCategory(HttpServletRequest httpServletRequest, @RequestParam  Integer id) {

        return ResponseEntity.ok(new ResponseDto(1, "Successfully", this.categoryService.deleteCategory(httpServletRequest, id)));
    }
}

package com.hackathon.rest;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hackathon.dto.request.QuestionRequestDto;
import com.hackathon.dto.response.ResponseDto;
import com.hackathon.service.QuestionService;

@RestController
public class QuestionController {

	@Autowired
	private QuestionService questionService;

	@GetMapping({"/api/question/listAllQuestion"})
	public ResponseEntity getListQuestion(HttpServletRequest httpServletRequest) {

		return ResponseEntity.ok(new ResponseDto(1, "Successfully", this.questionService.getListQuestion(httpServletRequest)));
	}

	@GetMapping({"/api/question/listAll"})
	public ResponseEntity listAll(@RequestParam(value = "categoryId", required = false) Integer categoryId, @RequestParam(value = "level", required = false) Integer level) {

		return ResponseEntity.ok(new ResponseDto(1, "Successfully", this.questionService.listAll(categoryId, level)));
	}
	

    @PostMapping({"/api/question/addQuestion"})
    public ResponseEntity addQuestion(HttpServletRequest httpServletRequest, @RequestBody  QuestionRequestDto request) {

        return ResponseEntity.ok(new ResponseDto(1, "Successfully", this.questionService.addQuestion(httpServletRequest, request)));
    }

    @PostMapping({"/api/question/editQuestion"})
    public ResponseEntity editQuestion(HttpServletRequest httpServletRequest, @RequestBody  QuestionRequestDto request) {

        return ResponseEntity.ok(new ResponseDto(1, "Successfully", this.questionService.editQuestion(httpServletRequest, request)));
    }

    @PostMapping({"/api/question/deleteQuestion"})
    public ResponseEntity editCategory(HttpServletRequest httpServletRequest, @RequestParam  Integer id) {

        return ResponseEntity.ok(new ResponseDto(1, "Successfully", this.questionService.deleteQuestion(httpServletRequest, id)));
    }
    
    @GetMapping({"/api/question/get-question/{id}"})
    public ResponseEntity checkUser(@PathVariable("id") String id) {
    	
        return ResponseEntity.ok(new ResponseDto(1, "Successfully", this.questionService.getQuestion(Integer.valueOf(id))));
    }
}

package com.hackathon.rest;

import com.hackathon.config.JwtTokenProvider;
import com.hackathon.dto.request.AuthenticationRequestDto;
import com.hackathon.dto.response.AuthTokenResponseDto;
import com.hackathon.dto.response.ResponseDto;
import com.hackathon.model.RoleEnum;
import com.hackathon.model.Token;
import com.hackathon.model.User;
import com.hackathon.repository.TokenRepository;
import com.hackathon.service.RoleService;
import com.hackathon.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping({"/api"})
public class AuthenticationController {
    private static final Logger logger = LoggerFactory.getLogger(AuthenticationController.class);
    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private JwtTokenProvider jwtTokenProvider;
    @Autowired
    private UserService userService;
    @Autowired
    private RoleService roleService;
    @Autowired
    private TokenRepository tokensRepository;
    @Value("${security.jwt.token.expire-length:3600000}")
    private long validityInMilliseconds = 3600000L;

    @PostMapping({"/login"})
    public ResponseEntity login(@RequestBody AuthenticationRequestDto data) {
        logger.debug("User login info : {}", data.toString());
        try {
            String username = data.getUsername();

            this.authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, data.getPassword()));

            User user = this.userService.getUserInfo(username);

            String token = this.jwtTokenProvider.createToken(username, Arrays.asList(new String[]{user.getRole().getName()}));

            Date now = new Date();
            Date validity = new Date(now.getTime() + this.validityInMilliseconds);
            saveToken(username, token, validity);
            String urlRedirect = "";
            String roleName = user.getRole().getName();
            int menuType = 0;
            switch (roleName) {
                case "ADMIN":
                    urlRedirect = "/admin/home-page";
                    menuType = 1;
                    break;
                case "HR":
                    urlRedirect = "/hr/home-page";
                    break;
                case "INTERVIEW":
                    urlRedirect = "/interview/home-page";
                    break;
                default:
                    urlRedirect = "/403";
                    break;
            }

            return ResponseEntity.ok(new ResponseDto(1, "Successfully", new AuthTokenResponseDto(token, username, urlRedirect, menuType)));
        } catch (AuthenticationException e) {
            throw new BadCredentialsException("Invalid username/password supplied");
        }
    }

    void saveToken(String userName, String tokenStr, Date expDate) {
        Token token = new Token();
        token.setUsername(userName);
        token.setExpDate(expDate);
        token.setToken(tokenStr);
        token.setActive(true);
        token.setUpdatedBy(userName);
        token.setCreatedBy(userName);
        this.tokensRepository.save(token);
    }

    @RequestMapping(value = {"/logout"}, method = {org.springframework.web.bind.annotation.RequestMethod.POST})
    public ResponseEntity logout(HttpServletRequest request) {
        String token = request.getHeader("Authorization");
        logger.debug("User logout token : {}", token);
        if (StringUtils.isEmpty(token)) {
            throw new BadCredentialsException("Not found token.");
        }
        Token tokens = this.tokensRepository.findByToken(token.replace("Bearer ", ""));
        if (tokens != null) {
            tokens.setActive(false);
        }
        this.tokensRepository.save(tokens);
        Map<Object, Object> model = new HashMap();
        model.put("msg", "Logout Successfully!");
        return ResponseEntity.ok(model);
    }
}

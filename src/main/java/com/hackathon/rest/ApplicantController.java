package com.hackathon.rest;

import com.hackathon.dto.request.AddApplicantRequestDto;
import com.hackathon.dto.request.uploadCvRequestDto;
import com.hackathon.dto.response.ResponseDto;
//import com.hackathon.dto.response.UploadFileResponseDto;
import com.hackathon.error.ProductException;
import com.hackathon.model.Applicant;
import com.hackathon.service.ApplicantService;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Objects;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
public class ApplicantController {
    @Autowired
    private ApplicantService applicantService;

    @GetMapping({"/api/applicant/listApplicant"})
    public ResponseEntity listInteviewer(HttpServletRequest httpServletRequest) {

        return ResponseEntity.ok(new ResponseDto(1, "Successfully", this.applicantService.listApplicant(httpServletRequest)));
    }

    @GetMapping({"/api/interview/applicant"})
    public ResponseEntity listInterviewerApplicant() {

        return ResponseEntity.ok(new ResponseDto(1, "Successfully", applicantService.listInterviewerApplicant()));
    }

    @PostMapping("/api/applicant/uploadCV")
    public ResponseEntity uploadCV(@RequestPart("file") MultipartFile file, @RequestPart("user") AddApplicantRequestDto user) throws IOException {

        Applicant applicant = this.applicantService.addApplicant(file, user);

        return ResponseEntity.ok(new ResponseDto(1, "Successfully", applicant));
    }


    @PostMapping("/api/applicant/editCV")
    public ResponseEntity editCV(@RequestPart(name = "file", required = false) MultipartFile file, @RequestPart("user") AddApplicantRequestDto user) throws IOException {

        Applicant applicant = this.applicantService.editApplicant(file, user);

        return ResponseEntity.ok(new ResponseDto(1, "Successfully", applicant));
    }

    @GetMapping("/api/applicant/{id}")
    public ResponseEntity getById(@PathVariable("id") Integer id) throws IOException {
        return ResponseEntity.ok(new ResponseDto(1, "Successfully", this.applicantService.getAptData(id)));
    }

    @PostMapping("/api/applicant/deleteCV")
    public ResponseEntity editCV(@RequestPart("id") String id) throws IOException {

        Applicant applicant = this.applicantService.deleteApplicant(id);

        return ResponseEntity.ok(new ResponseDto(1, "Successfully", applicant));
    }

    @RequestMapping(value = "/api/applicant/getpdf", method = RequestMethod.GET)
    public ResponseEntity getPDF1(@RequestParam("apt-id") Integer aptId) throws FileNotFoundException {

        Applicant applicant = applicantService.getAptData(aptId);
        if (Objects.isNull(applicant)) {
            throw new ProductException("Info not found!!!");
        }
        File file = new File(applicant.getCvPath());
        HttpHeaders headers = new HttpHeaders();
        headers.add("content-disposition", "inline;filename=" + file.getName());

        InputStreamResource resource = new InputStreamResource(new FileInputStream(file));

        return ResponseEntity.ok()
                .headers(headers)
                .contentLength(file.length())
                .contentType(MediaType.parseMediaType("application/pdf"))
                .body(resource);
    }
}

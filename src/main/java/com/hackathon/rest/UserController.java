package com.hackathon.rest;

import com.hackathon.dto.request.*;
import com.hackathon.dto.response.ResponseDto;
import com.hackathon.dto.response.UserResponseDto;
import com.hackathon.error.ProductException;
import com.hackathon.model.User;
import com.hackathon.service.UserService;
import com.hackathon.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping({"/api/user"})
public class UserController {
    private static final Logger logger = LoggerFactory.getLogger(UserController.class);
    @Autowired
    private UserService userService;

    @Autowired
    private Utils utils;

    @PostMapping({"/register"})
    public ResponseEntity register(HttpServletRequest req, @RequestBody UserRequestDto userRequestDto) {
        logger.debug("User register account.");
        logger.debug("Request : {}", userRequestDto.toString());

        UserResponseDto userResponseDto = this.userService.register(userRequestDto);

        return ResponseEntity.ok(new ResponseDto(1, "Successfully", userResponseDto));
    }

    @GetMapping({"/profile"})
    public ResponseEntity getProfile(HttpServletRequest request) {
        UserResponseDto userResponseDto = this.userService.getProfile(getToken(request));
        return ResponseEntity.ok(new ResponseDto(1, "Successfully", userResponseDto));
    }

    @PostMapping({"/profile"})
    public ResponseEntity updateProfile(HttpServletRequest request, @RequestBody UserRequestDto userRequestDto) {
        UserResponseDto userResponseDto = this.userService.updateProfile(getToken(request), userRequestDto);
        return ResponseEntity.ok(new ResponseDto(1, "Successfully", userResponseDto));
    }

    @PostMapping({"/add-member"})
    public ResponseEntity addMember(HttpServletRequest request, @RequestBody MemberRequestDto requestDto) {
        this.userService.addMember(getToken(request), requestDto);
        return ResponseEntity.ok(new ResponseDto(1, "Successfully"));
    }

    @PostMapping({"/delete-member/{member-id}"})
    public ResponseEntity deleteMember(@PathVariable("member-id") Integer memberId, HttpServletRequest request) {
        this.userService.deleteMember(memberId, request);
        return ResponseEntity.ok(new ResponseDto(1, "Successfully"));
    }

    @PostMapping({"/update-member/{member-id}"})
    public ResponseEntity updateMember(@PathVariable("member-id") Integer memberId, HttpServletRequest request, @RequestBody MemberRequestDto requestDto) {
        this.userService.updateMember(memberId, requestDto);
        return ResponseEntity.ok(new ResponseDto(1, "Successfully"));
    }

    @GetMapping({"/get-member/{member-id}"})
    public ResponseEntity getMember(@PathVariable("member-id") Integer memberId) {
        return ResponseEntity.ok(new ResponseDto(1, "Successfully", this.userService.getMember(memberId)));
    }

    @GetMapping({"/interview/list-all"})
    public ResponseEntity interviewListAll() {
        return ResponseEntity.ok(new ResponseDto(1, "Successfully", userService.interviewListAll()));
    }

    @GetMapping({"/listAll"})
    public ResponseEntity listAll(HttpServletRequest request) {
        return ResponseEntity.ok(new ResponseDto(1, "Successfully", userService.listAllMember(getToken(request))));
    }

    @PostMapping({"/reset-password"})
    public ResponseEntity resetPassword(@RequestBody ResetPasswordRequestDto requestDto) {
        return ResponseEntity.ok(new ResponseDto(1, "Successfully", this.userService.resetPassword(requestDto)));
    }

    @PostMapping({"/change-password"})
    public ResponseEntity changePassword(@RequestBody ChangePasswordReqDto requestDto) {
        this.userService.changePassword(requestDto);
        return ResponseEntity.ok(new ResponseDto(1, "Successfully"));
    }

    @PostMapping({"/changePassword"})
    public ResponseEntity changePassword(HttpServletRequest request, @RequestBody ChangePasswordRequestDto requestDto) {
        UserResponseDto userResponseDto = this.userService.changePassword(getToken(request), requestDto);
        return ResponseEntity.ok(new ResponseDto(1, "Successfully", userResponseDto));
    }

    private String getToken(HttpServletRequest request) {
        return request.getHeader("Authorization").replace("Bearer ", "");
    }

    @GetMapping({"/check-user/{user-name}"})
    public ResponseEntity checkUser(@PathVariable("user-name") String userName) {
        User user = userService.getUserInfo(userName);
        if(user == null) {
            throw new ProductException("Không tìm thấy thông tin");
        }
        return ResponseEntity.ok(new ResponseDto(1, "Successfully", user.getPoint()));
    }
}

package com.hackathon.rest;

import com.hackathon.config.JwtTokenProvider;
import com.hackathon.dto.request.AddApplicantRequestDto;
import com.hackathon.dto.request.AuthenticationRequestDto;
import com.hackathon.dto.request.UserRequestDto;
import com.hackathon.dto.response.InterviewDataResponseDto;
import com.hackathon.dto.response.UserResponseDto;
import com.hackathon.model.Applicant;
import com.hackathon.model.Category;
import com.hackathon.model.Token;
import com.hackathon.model.User;
import com.hackathon.repository.TokenRepository;
import com.hackathon.service.ApplicantService;
import com.hackathon.service.CategoryService;
import com.hackathon.service.InterviewDetailService;
import com.hackathon.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Controller
public class WebController {

    @Autowired
    private UserService userService;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenProvider jwtTokenProvider;

    @Autowired
    private TokenRepository tokensRepository;

    @Autowired
    private ApplicantService applicantService;

    @Autowired
    private InterviewDetailService interviewDetailService;

    @Autowired
    private CategoryService categoryService;
    
    @Value("${security.jwt.token.expire-length:3600000}")
    private long validityInMilliseconds = 3600000L;

    private Integer ONE_VALUE = 1;

    @GetMapping(value = {"/", "/index"})
    public String index(Model model) {
        return "index";
    }

    @GetMapping(value = {"/admin/home-page"})
    public String home(Model model) {
        return "/admin/home-page";
    }

    @GetMapping(value = {"/admin/member"})
    public String adminMember(Model model) {
        return "/admin/member";
    }

    @GetMapping(value = {"/interview/home-page"})
    public String interviewHome(Model model) {
        return "interview-homepage";
    }

    @GetMapping(value = {"/hr/home-page"})
    public String aptList(Model model) {
        return "hr-homepage";
    }
    
    @GetMapping(value = {"/hr/interview-create"})
    public String interviewCreate(Model model) {
        return "interview-create";
    }

    @GetMapping(value = {"/applicant/add"})
    public String aptAdd(Model model, @ModelAttribute AddApplicantRequestDto addApplicantRequestDto) {
        return "applicantAdd";
    }

    @GetMapping(value = {"/applicant/edit"})
    public String aptEdit(Model model, @RequestParam("id") Integer id) {
    	Applicant applicant = applicantService.getAptData(id);
    	model.addAttribute("applicantData", applicant);
        return "applicantEdit";
    }

    @GetMapping(value = {"/interview/input"})
    public String interviewInput(Model model, @RequestParam("id") Integer id) {
    	InterviewDataResponseDto interviewData = interviewDetailService.getInterviewData(id);
    	model.addAttribute("interviewData", interviewData);
        return "interview-input";
    }

    @GetMapping(value = {"/interview/view-details"})
    public String interviewViewDetails(Model model, @RequestParam("id") Integer id) {
    	InterviewDataResponseDto interviewData = interviewDetailService.getInterviewData(id);
    	model.addAttribute("interviewData", interviewData);
        return "view-details";
    }
    
    @GetMapping(value = {"/hr/view-details"})
    public String hrViewDetails(Model model, @RequestParam("id") Integer id) {
    	InterviewDataResponseDto interviewData = interviewDetailService.getInterviewData(id);
    	model.addAttribute("interviewData", interviewData);
        return "hr-view-details";
    }

    @GetMapping(value = {"/category"})
    public String categoryList(Model model) {
        return "category-list";
    }

    @GetMapping(value = {"/category/add"})
    public String categoryAdd(Model model) {
    	Category ca = new Category();
    	model.addAttribute("category", ca);
    	model.addAttribute("categoryId", "");
        return "categoryAdd";
    }

    @GetMapping(value = {"/category/edit"})
    public String categoryEdit(Model model, @RequestParam("id") Integer id) {
    	Category ca = categoryService.findById(id);
    	model.addAttribute("category", ca);
    	model.addAttribute("categoryId", id);
        return "categoryAdd";
    }
    
    @GetMapping(value = {"/log-out"})
    public String logout(Model model) {
        return "logout";
    }

    @GetMapping(value = {"/charge"})
    public String charge(Model model) {
        return "charge";
    }

    @RequestMapping(value = {"/login"}, method = {RequestMethod.GET})
    public String login(Model model, @ModelAttribute AuthenticationRequestDto authenticationRequestDto) {
        return "login";
    }

    @RequestMapping(value = {"/login"}, method = {RequestMethod.POST})
    public String submitLogin(Model model, @ModelAttribute AuthenticationRequestDto authenticationRequestDto, HttpSession session) {

        String username = authenticationRequestDto.getUsername();

        try {
            this.authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, authenticationRequestDto.getPassword()));
        } catch (Exception ex) {
            model.addAttribute("errorMessage", "Tài khoản hoặc mật khẩu không chính xác");
            return "/login";
        }

        User user = this.userService.getUserInfo(username);

        String token = this.jwtTokenProvider.createToken(username, Arrays.asList(new String[]{user.getRole().getName()}));
        Date now = new Date();
        Date validity = new Date(now.getTime() + this.validityInMilliseconds);
        saveToken(username, token, validity);
        session.setAttribute("token", token);
        return "home-page";
    }

    @GetMapping(value = {"/register"})
    public String register(Model model, @ModelAttribute UserRequestDto userRequestDto) {
        if (model.getAttribute("errorMessage") != null) {
            System.out.println(model.getAttribute("errorMessage"));
        }
        return "register";
    }

    @PostMapping(value = {"/register"})
    public ModelAndView submitRegister(ModelMap model, @ModelAttribute UserRequestDto userRequestDto) {
        UserResponseDto responseDto = userService.register(userRequestDto);
        if (responseDto == null) {
            model.addAttribute("errorMessage", "Tài khoản đã tồn tại trên hệ thống");
            return new ModelAndView("/register", model);
        }
        model.addAttribute("errorMessage", "Đăng ký thành công, mời đăng nhập");
        return new ModelAndView("forward:/login", model);
    }

    @GetMapping(value = {"/forgot-password"})
    public String forgotPassword(Model model) {
        return "forgot-password";
    }

    @GetMapping(value = {"/reset-password"})
    public String resetPassword(Model model) {
        return "reset-password";
    }

    void saveToken(String userName, String tokenStr, Date expDate) {
        Token token = new Token();
        token.setUsername(userName);
        token.setExpDate(expDate);
        token.setToken(tokenStr);
        token.setActive(true);
        token.setUpdatedBy(userName);
        token.setCreatedBy(userName);
        this.tokensRepository.save(token);
    }
}

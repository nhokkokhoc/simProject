package com.hackathon.rest;

import com.hackathon.dto.request.InterviewScheduleRequestDto;
import com.hackathon.dto.request.InterviewScheduleResultRequestDto;
import com.hackathon.dto.response.ResponseDto;
import com.hackathon.service.InterviewDetailService;
import com.hackathon.service.InterviewScheduleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;

@RestController
public class InterviewScheduleController {

    @Autowired
    private InterviewScheduleService service;

    @PostMapping({"/api/interview-schedule/result/{schedule-id}"})
    public ResponseEntity addResult(HttpServletRequest httpServletRequest, @PathVariable("schedule-id") Integer scheduleId, @RequestBody InterviewScheduleResultRequestDto request) {
        service.addResult(httpServletRequest, scheduleId, request);
        return ResponseEntity.ok(new ResponseDto(1, "Successfully"));
    }

    @PostMapping({"/api/interview-schedule/add-new"})
    public ResponseEntity addNew(HttpServletRequest httpServletRequest, @RequestBody InterviewScheduleRequestDto request) throws ParseException {
        service.addNew(httpServletRequest, request);
        return ResponseEntity.ok(new ResponseDto(1, "Successfully"));
    }

    @GetMapping({"/api/interview-schedule/get/{schedule-id}"})
    public ResponseEntity getToUpdate(@PathVariable("schedule-id") Integer scheduleId) throws ParseException {
        return ResponseEntity.ok(new ResponseDto(1, "Successfully", service.getToUpdate(scheduleId)));
    }

    @PutMapping({"/api/interview-schedule/{schedule-id}"})
    public ResponseEntity updateSchedule(HttpServletRequest httpServletRequest,
                                         @PathVariable("schedule-id") Integer scheduleId,
                                         @RequestBody InterviewScheduleRequestDto request) throws ParseException {
        service.updateSchedule(httpServletRequest, scheduleId, request);
        return ResponseEntity.ok(new ResponseDto(1, "Successfully"));
    }
    
    @GetMapping({"/api/interview-schedule/getlist"})
    public ResponseEntity getList()  {
        return ResponseEntity.ok(new ResponseDto(1, "Successfully", service.getList()));
    }
}

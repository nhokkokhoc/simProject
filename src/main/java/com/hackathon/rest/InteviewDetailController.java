package com.hackathon.rest;

import com.hackathon.dto.response.ResponseDto;
import com.hackathon.service.InterviewDetailService;
import com.hackathon.service.InteviewerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
public class InteviewDetailController {
    @Autowired
    private InterviewDetailService interviewDetailService;

    @GetMapping({"/api/interview-question"})
    public ResponseEntity findInteviewQuestion(HttpServletRequest httpServletRequest, @RequestParam("id") Integer id) {

        return ResponseEntity.ok(new ResponseDto(1, "Successfully", interviewDetailService.getQuestionData(httpServletRequest, id)));
    }
    
    @GetMapping({"/api/interview-schedule/detail"})
    public ResponseEntity findInteviewDetail(HttpServletRequest httpServletRequest, @RequestParam("id") Integer id) {

        return ResponseEntity.ok(new ResponseDto(1, "Successfully", interviewDetailService.getDetailData(httpServletRequest, id)));
    }
}

package com.hackathon.rest;

import com.hackathon.dto.response.ResponseDto;
import com.hackathon.service.InteviewerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
public class InteviewerController {
    @Autowired
    private InteviewerService inteviewerService;

    @GetMapping({"/api/interviewHome/listInteview"})
    public ResponseEntity listInteview(HttpServletRequest httpServletRequest) {
    	
    	return ResponseEntity.ok(new ResponseDto(1, "Successfully", this.inteviewerService.listInteview(httpServletRequest)));
    }
    
    @GetMapping({"/api/interview/listInteviewer"})
    public ResponseEntity listInteviewer(HttpServletRequest httpServletRequest) {
    	
    	return ResponseEntity.ok(new ResponseDto(1, "Successfully", this.inteviewerService.listInteviewer(httpServletRequest)));
    }

    @GetMapping({"/api/interview-schedule/{id}"})
    public ResponseEntity findById(@PathVariable("id") Integer id, HttpServletRequest httpServletRequest) {

        return ResponseEntity.ok(new ResponseDto(1, "Successfully", this.inteviewerService.findById(id)));
    }
}

package com.hackathon.utils;

public class Constants
{
    public static final String ROLE_USER = "USER";
    public static final String ROLE_ADMIN = "ADMIN";
    public static final String USER_ADMIN = "admin";
    public static final String HEADER_STRING = "Authorization";
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final int ACTIVE = 1;
    public static final int INACTIVE = 0;
    public static final int SUCCESS = 1;
    public static final int ERROR = 0;
    public static final int LICENSE_KEY_LENGTH = 15;
    public static final String DATE_TIME_FORMAT_YYYY_MM_DD = "yyyy-MM-dd";
    public static final String DATE_TIME_FORMAT_YYYY_MM_DD_HH_MM_SS = "yyyy-MM-dd hh:mm:ss";
    public static final String DATE_TIME_FORMAT_YYYY_MM_DD_HH_MM_A = "yyyy-MM-dd hh:mm a";
    public static final int PENDING = 0;
    public static final int APPROVE = 1;
    public static final int REJECT = 2;
    public static final String ADD_POINT = "ADD_POINT";
    public static final String BUY_GOLD = "BUY_GOLD";
    public static final String RATE_CHARGING_KEY = "RATE_CHARGING_KEY";
    public static final String RATE_CHARGING_VALUE = "1";
    public static final String SO_POINT_KHONG_DU_DE_DOI_VANG = "S? point c?a b?n kh�ng ?? ?? ??i, vui l�ng n?p th�m.";
    public static final String REFERER = "Referer";
    public static final String CARD = "card";
    public static final String AMOUNT = "amount";
    public static final String SERIAL = "serial";
    public static final String PIN = "pin";
    public static final String API_KEY = "api_key";
    public static final String API_SECRET = "api_secret";
    public static final String CONTENT = "content";
    public static final String NAP_CARD = "NAP_CARD";
}

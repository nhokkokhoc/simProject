package com.hackathon.utils;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class RestUtils
{
    private static final String USER_AGENT = "Mozilla/5.0";

    public static String sendPost(String url)
            throws Exception
    {
        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection)obj.openConnection();

        con.setRequestMethod("POST");
        con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
        con.setRequestProperty("User-Agent", "Mozilla/5.0");
        int responseCode = con.getResponseCode();

        System.out.println("\nSending 'POST' request to URL : " + url);
        System.out.println("Response Code : " + responseCode);

        BufferedReader inPut = new BufferedReader(new InputStreamReader(con.getInputStream()));

        StringBuffer result = new StringBuffer();
        String inputLine;
        while ((inputLine = inPut.readLine()) != null) {
            result.append(inputLine);
        }
        inPut.close();
        return result.toString();
    }

    public static String sendPostFormData(String url, String referer, String card, String amount, String serial, String pin, String apiKey, String apiSecret, String content)
            throws Exception
    {
        HttpClient client = new DefaultHttpClient();
        HttpPost post = new HttpPost(url);

        post.setHeader("Referer", referer);

        List<NameValuePair> urlParameters = new ArrayList();
        urlParameters.add(new BasicNameValuePair("card", card));
        urlParameters.add(new BasicNameValuePair("amount", amount));
        urlParameters.add(new BasicNameValuePair("serial", serial));
        urlParameters.add(new BasicNameValuePair("pin", pin));
        urlParameters.add(new BasicNameValuePair("api_key", apiKey));
        urlParameters.add(new BasicNameValuePair("api_secret", apiSecret));
        urlParameters.add(new BasicNameValuePair("content", content));

        post.setEntity(new UrlEncodedFormEntity(urlParameters));

        HttpResponse response = client.execute(post);
        System.out.println("\nSending 'POST' request to URL : " + url);
        System.out.println("Post parameters : " + post.getEntity());
        System.out.println("Response Code : " + response
                .getStatusLine().getStatusCode());

        BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

        StringBuffer result = new StringBuffer();
        String line = "";
        while ((line = rd.readLine()) != null) {
            result.append(line);
        }
        System.out.println(result.toString());
        return result.toString();
    }

    public static String sendPost(String url, String data, int packageCharge)
            throws Exception
    {
        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection)obj.openConnection();

        con.setRequestMethod("POST");
        con.setRequestProperty("User-Agent", "Mozilla/5.0");
        con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
        con.setConnectTimeout(2000);
        con.setDoOutput(true);
        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
        wr.writeBytes(data);
        wr.flush();
        wr.close();

        int responseCode = con.getResponseCode();
        System.out.println("\nSending 'POST' request to URL : " + url);
        System.out.println("Post parameters : " + data);
        System.out.println("Response Code : " + responseCode);

        if(responseCode == 200 && (packageCharge == 7 || packageCharge == 11)) {
            return "Successfully";
        }

        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));

        StringBuffer response = new StringBuffer();
        String inputLine;
        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        System.out.println(response.toString());
        return response.toString();
    }

//    public static void main(String[] args)
//            throws Exception
//    {
//        String url = "http://103.90.225.183:7001/recharge/manual.do";
//        String request = "{\"roleId\":100157,\"serverId\":4001,\"rechargeId\":1,\"number\":1}";
//        String response = sendPost(url, request);
//    }
}

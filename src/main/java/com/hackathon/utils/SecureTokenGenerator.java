package com.hackathon.utils;

import java.security.SecureRandom;

public class SecureTokenGenerator {
    public static final String CHARACTERS = "abcdefghijklmnopqrstuvwxyz0123456789";

    public static final int SECURE_TOKEN_LENGTH = 32;

    private static final SecureRandom random = new SecureRandom();

    private static final char[] symbols = CHARACTERS.toCharArray();

    private static final char[] buf = new char[SECURE_TOKEN_LENGTH];

    /**
     * Generate the next secure random token in the series.
     */
    public static String nextToken() {
        for (int idx = 0; idx < buf.length; ++idx)
            buf[idx] = symbols[random.nextInt(symbols.length)];
        return new String(buf);
    }

    public static void main(String[] args) {
        System.out.println(nextToken());
    }
}

package com.hackathon.utils;

import com.hackathon.error.ProductException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.sql.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;

@Component
public class Utils {
    @Value("${folder.upload.url.root}")
    private String urlRoot;
    private static final String ALPHA_NUMERIC_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    private static final String NUMERIC_STRING = "0123456789";

    private static Date getToday() {
        Calendar day = Calendar.getInstance();
        day.set(Calendar.MILLISECOND, 0);
        day.set(Calendar.SECOND, 0);
        day.set(Calendar.MINUTE, 0);
        day.set(Calendar.HOUR_OF_DAY, 0);
        return day.getTime();
    }

    public String write(MultipartFile file, String urlPath)
            throws IOException {
        File directory = new File(urlRoot + urlPath);
        if (!directory.exists()) {
            boolean linux = directory.mkdirs();
            if (!linux) {
                directory.mkdir();
            }
        }
        String date = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyMMddHHmmss_"));
        String pathSave = urlPath + "/" + date + file.getOriginalFilename();
        String realPathToUploads = directory + "/" + date + file.getOriginalFilename();

        File fileSave = new File(realPathToUploads);
        file.transferTo(fileSave);
        return pathSave;
    }

    public static String randomAlphaNumeric(int count) {
        StringBuilder builder = new StringBuilder();
        while (count-- != 0) {
            int character = (int) (Math.random() * "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789".length());
            builder.append("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789".charAt(character));
        }
        return builder.toString();
    }

    public static String randomNumeric(int count) {
        StringBuilder builder = new StringBuilder();
        while (count-- != 0) {
            int character = (int) (Math.random() * NUMERIC_STRING.length());
            builder.append(NUMERIC_STRING.charAt(character));
        }
        return builder.toString();
    }

    public static Date plusDay(int numberDay) {
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        c.add(5, numberDay);
        return c.getTime();
    }

    public static String getToken(HttpServletRequest request) {
        return request.getHeader("Authorization").replace("Bearer ", "");
    }

    public static Connection getConnection(String dbURL, String userName,
                                           String password) {
        Connection conn = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(dbURL, userName, password);
            System.out.println("connect successfully!");
        } catch (Exception ex) {
            System.out.println("connect failure!");
            ex.printStackTrace();
        }
        return conn;
    }

}

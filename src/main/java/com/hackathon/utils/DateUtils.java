package com.hackathon.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtils
{
    private static final Logger LOG = LoggerFactory.getLogger(DateUtils.class);
    public static final String DDMMYYYY = "dd/MM/yyyy";
    public static final String YYYYMMDD = "yyyy/MM/dd";
    public static final String YYYYDDMMHHMM = "yyyy-MM-dd HH:mm";
    public static final String DDMMYYYYHHMMSS = "dd/MM/yyyy HH:mm:ss";

    public static String getDateTimeNow(String format)
    {
        SimpleDateFormat df = new SimpleDateFormat(format);
        return df.format(new Date());
    }

    public static String convertDateToString(Date date, String format)
    {
        SimpleDateFormat df = new SimpleDateFormat(format);
        return df.format(date);
    }

    public static Date convertStringToDate(String format, String strDate)
    {
        SimpleDateFormat df = new SimpleDateFormat(format);
        try
        {
            return df.parse(strDate);
        }
        catch (ParseException e)
        {
            LOG.error("Exception! Error parse string Date to Date");
        }
        return null;
    }

    public static Date plusDay(int day)
    {
        Date date = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(5, day);
        return c.getTime();
    }

    public static Date plusHour(int hour)
    {
        Date date = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(10, hour);
        return c.getTime();
    }

    public static Date plusMinute(int minute)
    {
        Date date = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(12, minute);
        return c.getTime();
    }

    public static Date plusMinuteDateOption(Date date, int minute)
    {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(12, minute);
        return c.getTime();
    }

    public static Date plusDayDateOption(Date date, int day)
    {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(5, day);
        return c.getTime();
    }

    public static Timestamp stringToTimeStamp(String param)
    {
        try
        {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            Date parsedDate = dateFormat.parse(param);
            return new Timestamp(parsedDate.getTime());
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return null;
    }

    public static Date stringToDate(String param)
    {
        try
        {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            return dateFormat.parse(param);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return null;
    }
}

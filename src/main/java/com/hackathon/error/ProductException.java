package com.hackathon.error;

public class ProductException extends RuntimeException {
    private static final long serialVersionUID = 1L;
    private String message;

    public String getMessage() {
        return this.message;
    }

    public ProductException() {
    }

    public ProductException(String message) {
        this.message = message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}

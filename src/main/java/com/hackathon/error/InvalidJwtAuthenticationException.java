package com.hackathon.error;

public class InvalidJwtAuthenticationException extends RuntimeException {
    private String message;

    public InvalidJwtAuthenticationException(String message) {
        super(message);
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}

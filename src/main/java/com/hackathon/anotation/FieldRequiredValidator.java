package com.hackathon.anotation;

import com.hackathon.error.ProductException;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class FieldRequiredValidator
        implements ConstraintValidator<FieldRequired, Object>
{
    public void initialize(FieldRequired constraintAnnotation) {}

    public boolean isValid(Object value, ConstraintValidatorContext context)
    {
        if (value == null) {
            throw new ProductException("All field required.");
        }
        if (((value instanceof String)) && (value.toString().trim().isEmpty())) {
            throw new ProductException("All field required.");
        }
        if (((value instanceof Integer)) && (((Integer)value).intValue() == 0)) {
            throw new ProductException("All field required.");
        }
        return true;
    }
}
